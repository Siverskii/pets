<?php
require 'backEnd/vendor/autoload.php';
spl_autoload_register(function ($classname) {
    require ("backEnd/classes/" . $classname . ".php");
});
$app = new Slim\App();

$container = $app->getContainer();
$container['view'] = new \Slim\Views\PhpRenderer("frontEnd/");
$container['db'] = new dbWorker();
$container['petsFunc'] = new petsFunc();

$app->get('/', function ($request, $response, $args) {
    $response = $this->view->render($response, "index.phtml");
    return $response;
});


$app->get('/petsCollection', function ($request, $response, $args) {
    $petsCollection = $this->db->getPets($request->getQueryParams());
        if($petsCollection == "dbError"){
            return $newResponse =  $response -> withJson("dbError", 500);
        }else if(!$petsCollection){
            return $newResponse =  $response -> withJson("noData", 404);  
        }
    return  $newResponse = $response -> withJson($petsCollection, 200);
});

$app->get('/myPetsCollection', function ($request, $response, $args) {
    if ($uPrivateInfo = $this->petsFunc->tokenConfirm($_COOKIE["petsjwt"])){
        $petsCollection = $this->db -> getMyPets($uPrivateInfo ->uID);
        if($petsCollection == "error"){
            return $newResponse =  $response->withJson("dbError", 500);
        }
    }
    return  $newResponse = $response->withJson($petsCollection, 200);
});


$app->put('/likeToImg', function ($request, $response, $args) {
    $content = $request->getParsedBody();
    $petsId = filter_var($content['id'], FILTER_SANITIZE_STRING);
    if($petsId){
        $resp = $this->db -> addLikeToImg($petsId);
        if(!$resp){
            return $newResponse =  $response -> withJson("dbError", 500);
        }
        return $newResponse = $response -> withJson($resp, 200);
    }else{
        return $newResponse = $response -> withJson("idFail", 400);
    }   
});

$app->get('/getAllPetImage', function ($request, $response, $args) {
     if(!$allPetImage = $this->db ->getAllPetImage($request->getQueryParams()["id"])){
        return $newResponse =  $response -> withJson("dbError", 500);    
     }
    return  $newResponse = $response->withJson($allPetImage, 200);
});
$app->get('/getUserContact', function ($request, $response, $args) {
     if ($uPrivateInfo = $this->petsFunc->tokenConfirm($_COOKIE["petsjwt"])){
         if(!$usersContact = $this->db ->getUserContact($request->getQueryParams()["id"])){
            return $newResponse =  $response -> withJson("dbError", 500);    
         }
         if($usersContact[emailEn] !="0" || $usersContact[phoneEn] !="0")
             return  $newResponse = $response->withJson($usersContact, 200);
         else
             return  $newResponse = $response->withJson("noCont", 500);
     }  
    return $newResponse =  $response -> withJson("autFail", 500);
});




$app->post('/petsUser', function ($request, $response, $args) {
    $content = $request->getParsedBody();
    $email = filter_var($content['email'], FILTER_SANITIZE_STRING);
    $password = filter_var($content['password'], FILTER_SANITIZE_STRING);
    if(!$userExist = $this->db->isUserExists($email)){
        if($uID = $this->db->addNewUser($email,$password)){
            $JWT = $this->petsFunc->generateJSWT($email, $uID); 
            setcookie('petsjwt', $JWT, time() + 2592000, '/', '', false, true);
            setcookie('petsaut', "true", time() + 2592000, '/', '', false, false);
            return $newResponse = $response -> withJson("Регистрация прошла успешно! Теперь Вы можете добавлять своих любимцев.", 200);
        }else{
          return $newResponse = $response -> withJson("Сбой соединения, пожалуйста проверьте подключение к интернету и повторите попытку", 500);  
        }
    }elseif($userExist != "dbError"){
       return $newResponse = $response -> withJson("Пользователь с Email: ".$email." уже существует!", 409); 
    }else{
      return $newResponse = $response -> withJson("Сбой соединения, пожалуйста проверьте подключение к интернету и повторите попытку", 500);    
    } 
});

$app->get('/petsUser', function ($request, $response, $args) {
    if ($uPrivateInfo = $this->petsFunc->tokenConfirm($_COOKIE["petsjwt"])){
        if(!$userInfo = $this->db->getUserInfo($uPrivateInfo ->uID)){
            return $newResponse =  $response -> withJson("dbError", 500);
        }
        return  $newResponse = $response -> withJson($userInfo, 200);   
    }else{
        return $newResponse =  $response -> withJson("autFail", 500);    
    }  
});

$app->put('/petsUser/{id}', function ($request, $response, $args) { 
    if ($uPrivateInfo = $this->petsFunc->tokenConfirm($_COOKIE["petsjwt"])){
        $content = $request->getParsedBody(); 
        $email = filter_var($content['email'], FILTER_SANITIZE_STRING);
                       
        if($email != $uPrivateInfo ->email){
            if(!$userExist = $this->db->isUserExists($email)){
                $JWT = $this->petsFunc->generateJSWT($email, $uPrivateInfo -> uID); 
                if($updateConfirm = $this->db->updateUserInfo($uPrivateInfo ->uID,$content)){
                    setcookie('petsjwt', $JWT, time() + 2592000, '/', '', false, true);
                    setcookie('petsaut', "true", time() + 2592000, '/', '', false, false);  
                    return  $newResponse = $response -> withJson("updateOK", 200);     
                }else{
                   return $newResponse =  $response -> withJson("dbError", 500);  
                }
            }elseif($userExist != "dbError"){
               return $newResponse = $response -> withJson("Пользователь с Email: ".$email." уже существует!", 409); 
            }else{
              return $newResponse = $response -> withJson("Сбой соединения, пожалуйста проверьте подключение к интернету и повторите попытку", 500);    
            }    
        }else{
           if($updateConfirm = $this->db->updateUserInfo($uPrivateInfo ->uID,$content)){ 
                return  $newResponse = $response -> withJson("updateOK", 200);     
            }else{
                   return $newResponse =  $response -> withJson("dbError", 500);  
            } 
        } 
    }else{
        return $newResponse =  $response -> withJson("autFail", 500);    
    }    
});
//2bab4c763ed0cbd594b923042953c58ef7b5c566623e940c1946834de0c02bec

$app->get('/userLogOut', function ($request, $response, $args) {
    setcookie('petsjwt', "false", time() - 2592000, '/', '', false, true);
    setcookie('petsaut', "false", time() - 2592000, '/', '', false, false);
    $newResponse = $response -> withJson("ulg_ok", 200);
    return $newResponse; 
});

$app->post('/userLogIn', function ($request, $response, $args) {
    $content = $request->getParsedBody();
    $email = filter_var($content['email'], FILTER_SANITIZE_STRING);
    $password = filter_var($content['password'], FILTER_SANITIZE_STRING);
    
    $uID = $this->db->userConfirm($email, $password);
    if(!$uID){
        return $newResponse = $response -> withJson("Неверный Email или пароль, попробуйте еще раз.", 409);    
    }elseif($uID != "dbError"){
        $JWT = $this->petsFunc->generateJSWT($email,$uID);
        setcookie('petsjwt', $JWT, time() + 2592000, '/', '', false, true);
        setcookie('petsaut', "true", time() + 2592000, '/', '', false, false);
        return $newResponse = $response -> withJson("Приветствуем Вас!", 200);
    }elseif($uID == "dbError"){
        return $newResponse = $response -> withJson("Сбой соединения, пожалуйста проверьте подключение к интернету и повторите попытку", 500);   
    }    
});
    
$app->get('/getDataFromFile', function ($request, $response, $args) {
    $data = $this->petsFunc->getDataFromFile($request->getQueryParams()['file']);
    if ($data){
        return  $newResponse =  $response -> withJson($data, 200);    
    }else{
       return  $newResponse =  $response -> withJson(false, 500);     
    }
});    
    
    
$app->get('/pets', function ($request, $response, $args) {
     $petsId = $request->getQueryParams()["petsId"];
        if($petDescr = $this->db->getPetDescription($petsId)){
            $uPrivateInfo = $this->petsFunc->tokenConfirm($_COOKIE["petsjwt"]);
            if ($uPrivateInfo ->uID == $petDescr["ownerID"])
               $petDescr["own"] = "1"; 
            else
               $petDescr["own"] = "0";
             return $newResponse = $response -> withJson($petDescr, 200);    
        }else{
            return $newResponse = $response -> withJson("Сбой соединения, пожалуйста проверьте подключение к интернету и повторите попытку", 500); 
        } 
});


$app->post('/pets', function ($request, $response, $args) {
    if ($uPrivateInfo = $this->petsFunc->tokenConfirm($_COOKIE["petsjwt"])){
        $content = $request->getParsedBody();
        $imageArray = $content['img'];
        $catOrDog = (filter_var($content['catsOrDogs'], FILTER_SANITIZE_STRING));
        
        if ($savedImage = $this->petsFunc->savePetsImage($imageArray, $uPrivateInfo ->uID, $catOrDog)){
            if($this->db->savePet($content,$savedImage,$uPrivateInfo ->uID)){
                return  $newResponse =  $response -> withJson($petsId, 200);    
            }else{
                return  $newResponse =  $response -> withJson("dbSaveError", 500);      
            }
        }else{
            return  $newResponse =  $response -> withJson("imgSaveFail", 500);     
        }
    }else{
        return $newResponse =  $response -> withJson("autFail", 500);    
    }
});    


$app->put('/pets/{id}', function ($request, $response, $args) {
    if ($uPrivateInfo = $this->petsFunc->tokenConfirm($_COOKIE["petsjwt"])){
        $content = $request->getParsedBody();
        $imageArray = $content['img'];
        $deleteImageArray = $content['deleteImgArray'];
        $catOrDog = (filter_var($content['catsOrDogs'], FILTER_SANITIZE_STRING));
        $savedImage = false;
        
        if ($imageArray){
            if (!$savedImage = $this->petsFunc->savePetsImage($imageArray, $uPrivateInfo ->uID, $catOrDog)){
                return  $newResponse =  $response -> withJson("imgSaveFail", 500); 
            }         
        }
        if($deleteImageArray){
            if(!$deletedImage = $this->db->deletePetsImage($deleteImageArray,$catOrDog)){
                 return  $newResponse =  $response -> withJson("imgDeleteFail", 500); 
            } 
        }
        if($this->db->updatePet($content,$uPrivateInfo ->uID, $savedImage)){
            return  $newResponse =  $response -> withJson("ok", 200);    
        }else{
            return  $newResponse =  $response -> withJson("dbSaveError", 500);      
        }
    }else{
        return $newResponse =  $response -> withJson("autFail", 500);        
    }
});
                

$app->delete('/pets/{id}', function ($request, $response, $args) {
    $petsId = $request->getAttribute("id");
     if ($uPrivateInfo = $this->petsFunc->tokenConfirm($_COOKIE["petsjwt"])){
        if($this->db->deletePet($petsId)){
             return $newResponse = $response -> withJson("ok", 200);    
        }else{
            return $newResponse = $response -> withJson("Сбой соединения, пожалуйста проверьте подключение к интернету и повторите попытку", 500); 
        }    
     }
});


$app->run();