({
    baseUrl: "frontEnd/app/js/",
    paths: {
        jquery: '../../libs/jquery',
        backbone: '../../libs/backbone',
        underscore: '../../libs/underscore',
        marionette: '../../libs/backbone.marionette',
        text: '../../libs/text',
        tpl: '../../libs/underscore-tpl',
        jqCookie: '../../libs/js.cookie',
        backboneRadio: "../../libs/backbone.radio"
    },
    dir: "appdirectory-build",
    shim: {
        underscore: {
            exports: '_'
        },
        backbone: {
            deps: ['underscore', 'jquery'],
            exports: 'Backbone'
        },
        backboneRadio: {
            deps: ['backbone'],
            exports: 'Radio'
        },
        marionette: {
            deps: ["backboneRadio"],
            exports: "Marionette"
        },
        tpl: ["text"],
        jqCookie: {
            deps: ['jquery'],
            exports: 'cookie'
        }
    },
    modules: [
        {
            name: "app",
            include: ["layout/layout", "pets/petsRouter", "common/waitingIndicator/waitingIndicator", "common/popUp/popUpController"],

        },
        {
            name: "showPets/showController",
            exclude: [
                "app", "jquery", "backbone", "underscore", "marionette", "text", "tpl", "jqCookie", "backboneRadio"
            ]
        },
        {
            name: "users/userController",
            exclude: [
                "app", "jquery", "backbone", "underscore", "marionette", "text", "tpl", "jqCookie", "backboneRadio"
            ]
        },

        {
            name: "searchPets/petsSearchController",
            exclude: [
                "app", "jquery", "backbone", "underscore", "marionette", "text", "tpl", "jqCookie", "backboneRadio", "petsForm/petsFormController"
            ]
        },
        {
            name: "myPets/myPetsController",
            exclude: [
                "app", "jquery", "backbone", "underscore", "marionette", "text", "tpl", "jqCookie", "backboneRadio", "petsForm/petsFormController"
            ]
        },
        {
            name: "pets/footer/about",
            exclude: [
                "app", "jquery", "backbone", "underscore", "marionette", "text", "tpl", "jqCookie", "backboneRadio"
            ]
        },
        {
            name: "petsForm/petsFormController",
            exclude: [
                "app", "jquery", "backbone", "underscore", "marionette", "text", "tpl", "jqCookie", "backboneRadio"
            ]
        },
    ]
})