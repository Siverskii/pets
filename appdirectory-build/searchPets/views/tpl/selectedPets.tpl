<% catOrDog == "1" ? type = "cats_thumb" : type = "dogs_thumb" %>
    <div class="petsWrapper">
        <div class="petsImgWrapper">
            <img src="data:<%=image[0]%>;base64,<%=image[1]%>" alt="Любимец">
        </div>
        <div class="petsName">
            <%= breed %>
        </div>
        <div class="petsCondition">
            <% if (sellLoveGift==="1"){ %>
                <div class="petsSale">
                    <%= price %> <i class="icon-rouble"></i>
                </div>
                <%}%>
                    <% if (sellLoveGift==="4"){ %>
                        <div class="petsSale">
                            <i class="icon-home"></i>
                        </div>
                        <%}%>
                            <% if (sellLoveGift==="2"){ %>
                                <div class="petsSale">
                                    <%= lovePrice %> <i class="icon-venus-mars"></i>
                                </div>
                                <%}%>
                                    <div class="petsLikes">
                                        <%= likes %> <i class="icon-heart"></i>
                                    </div>

        </div>
    </div>