<div class="popUpHeader <% if (popUptype == 'errorPopUp' ){%> errorNot <%} else if (popUptype == 'successPopUp'){%>okNot<%}%>">
    <p class="popUpTitle ">
        <%= title %>
    </p>
    <%if (popUptype != 'successPopUp'){%>
        <div class="closeIconWrapper">
            <i class="icon-cancel" aria-hidden="true"></i>
        </div>
        <%}%>
</div>
<div class="notificationBody">
    <% if(popUptype == "formPopUp") {%>
        <form onsubmit="return false">
            <div class="formFilds">
                <label for="formEmail">
                    Email: </label>
                <input id="formEmail" type="email" required autofocus />
            </div>
            <div class="formFilds">
                <label for="formEmail">
                    Пароль:</label>
                <input id="formPassword" type="password" required/>
                <% if (rememberPassword) { %>
                    <p class="remPassword"> Забыли пароль? </p>
                    <% } %>
            </div>
            <% }else{%>
                <%= body %>
                    <%}%>

                        <button class="reg_btn mainMenuButton<% if (popUptype=='errorPopUp' ){%> errorNot<%} else if (popUptype == 'successPopUp'){%> okNot<%}%>" type="submit">
                            <%= button %>
                        </button>
        </form>
</div>