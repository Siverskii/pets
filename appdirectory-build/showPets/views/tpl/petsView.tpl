<% catOrDog == "1" ? type = "cats" : type = "dogs" %>
    <img class="mainPetsImg" src="data:<%=image[0]%>;base64,<%=image[1]%>">

    <div class="likeDislikeButtonWrapper">
        <div id="dislike_btn" class="fa-times-Wrapper">
            <i class="fa fa-times icon-cancel"></i>
        </div>
        <div id="like_btn" class="fa-check-Wrapper">
            <i class="fa fa-check icon-ok"></i>
        </div>
    </div>