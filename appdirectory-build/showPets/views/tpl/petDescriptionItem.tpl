<form class="petsForm petsDescriptionForm" onsubmit="return false">
    <fieldset class="formSection">
        <div class="formItem">
            <input id="petsDescriptionForm__breed" class="formItem__input petsForm_BrownDecor" type="text" autocomplete="off" value="<%=breed%>">
            <label class="formItem__label petsForm_BrownDecor" for="petsDescriptionForm__breed">Порода:</label>
        </div>
        <div class="formItem">
            <input id="petsDescriptionForm__sort" class="formItem__input petsForm_BrownDecor" type="text" autocomplete="off" value="<%=(function(){if(MF==='0' ) return 'Мальчик'; else if (MF ==='1') return 'Девочка'}()) %>">
            <label class="formItem__label petsForm_BrownDecor" for="petsDescriptionForm__sort"> Пол: </label>
            <div class="getDataFromFileListParent"></div>
        </div>
        <div class="formItem">
            <input id="petsDescriptionForm__status" class="formItem__input petsForm_BrownDecor" type="tel" autocomplete="off" value="<%=(function(){if(sellLoveGift==='8' ) return 'Похвастаться'; if(sellLoveGift==='1' ) return 'Продается за ' + price; else if (sellLoveGift ==='2') return 'Ищет пару(Вязка)'; else if (sellLoveGift ==='4') return 'Ищет новый дом' }()) %>">
            <label class="formItem__label petsForm_BrownDecor" for="petsDescriptionForm__status"> Статус: </label>
        </div>
    </fieldset>
    <%if (descr){%>
        <fieldset class="formSection petsForm_BrownDecor formSection_textarea">
            <textarea id="addForm__descr" class="formItem__textarea petsForm_BrownDecor" autocomplete="off">
                <%=descr%>
            </textarea>
        </fieldset>
        <%}%>
            <div id="petsDescription_showOwnerContact" class="petsForm_BrownDecor mainMenuButton"> Показать контакты хозяина</div>
            <div id="petsDescription_closePets" class="petsForm_BrownDecor mainMenuButton"> Закрыть</div>
</form>