<form class="petsForm addForm" onsubmit="return false">
    <fieldset class="formSection petsForm_BrownDecor">
        <legend class="formSection__name">Какая у Вас цель?</legend>
        <div class="formItem formItem_formItemCon">
            <input id="searchForm__boast" class="formItem__input petsForm_BrownDecor" <%=(function(){if(sellLoveGift==="8" ) return "checked"}()) %> name="goal" type="radio" checked hidden />
            <label class="formItem__label petsForm_BrownDecor" for="searchForm__boast">Похвастаться</label>
            <input id="searchForm__byPets" class="formItem__input petsForm_BrownDecor" name="goal" <%=(function(){if(sellLoveGift==="1" ) return "checked"}()) %> type="radio" hidden />
            <label class="formItem__label petsForm_BrownDecor" for="searchForm__byPets">Продать</label>
            <input id="searchForm__love" class="formItem__input petsForm_BrownDecor" name="goal" <%=(function(){if(sellLoveGift==="2" ) return "checked"}()) %> type="radio" hidden />
            <label class="formItem__label petsForm_BrownDecor" for="searchForm__love">Ищу пару(вязка)</label>
            <input id="searchForm__getFree" class="formItem__input petsForm_BrownDecor" name="goal" <%=(function(){if(sellLoveGift==="4" ) return "checked"}()) %> type="radio" hidden />
            <label class="formItem__label petsForm_BrownDecor" for="searchForm__getFree">Отдать бесплатно</label>
        </div>
        <div class="formItem">
            <input id="addForm__price" class="formItem__input petsForm_BrownDecor" type="text" placeholder="Продажи или вязки" autocomplete="off" value="<%=price%>">
            <label class="formItem__label petsForm_BrownDecor" for="addForm__price">Стоимость:</label>
        </div>
    </fieldset>

    <fieldset class="formSection petsForm_BrownDecor">
        <legend class="formSection__name">Расскажите о любимце</legend>
        <div class="formItem formItem_formItemCon">
            <input id="searchForm__cat" class="formItem__input petsForm_BrownDecor" name="catsDogs" <%=(function(){if(catOrDog==="1") return "checked"}()) %> type="radio" hidden />
            <label class="formItem__label petsForm_BrownDecor"  for="searchForm__cat">Кошка</label>
            <input id="searchForm__dog" class="formItem__input petsForm_BrownDecor" <%=(function(){if(catOrDog==="0") return "checked"}()) %> name="catsDogs" type="radio" hidden />
            <label class="formItem__label petsForm_BrownDecor" for="searchForm__dog">Собака</label>
            <input id="searchForm__boy" class="formItem__input petsForm_BrownDecor" <%=(function(){if(MF==="0") return "checked"}())%>   name="girlBoy" type="radio" hidden />
            <label class="formItem__label petsForm_BrownDecor" for="searchForm__boy">Мальчик</label>
            <input id="searchForm__girl" class="formItem__input petsForm_BrownDecor" <%=(function(){if(MF==="1") return "checked"}())%>   name="girlBoy" type="radio" hidden />
            <label class="formItem__label petsForm_BrownDecor" for="searchForm__girl">Девочка</label>
        </div>
        <div class="formItem formItem_getDataFromFile">
            <input id="petsForm__breed" class="formItem__input petsForm_BrownDecor formItem__input_getDataFromFile" data-type="breed" type="text" autocomplete="off" value="<%=breed%>">
            <label class="formItem__label petsForm_BrownDecor" for="petsForm__breed"> Порода: </label>
            <div class="getDataFromFileListParent"></div>
        </div>
        <div class="formItem">
            <input id="petsForm__name" class="formItem__input petsForm_BrownDecor" type="text" autocomplete="off" value="<%=name%>">
            <label class="formItem__label petsForm_BrownDecor" for="petsForm__name">Имя:</label>
        </div>

    </fieldset>
    <fieldset class="formSection petsForm_BrownDecor formSection_textarea">
        <textarea id="addForm__descr" class="formItem__textarea petsForm_BrownDecor" autocomplete="off" placeholder="Расскажите подробнее о своем любимце"><%=descr%></textarea>
    </fieldset>


    <div class="allImageAddWrapper" id="OldimageWrapper"></div>

    <div class="allImageAddWrapper" id="newImageAddWrapper"> </div>
    <input type="file" id="imgUpload" hidden multiple>
    <label class="imageAddlabel mainMenuButton " for="imgUpload"></label>
    <div class="profFormBtnWrapper">
        <div>
            <div class="addFormButton mainMenuButton">Сохранить любимца</div>
        </div>
        <%if(id) {%>

            <div class="petDeleteBtn  mainMenuButton">Удалить любимца</div>
    </div>
    <%}%>
</form>