<div class="imgItemWrapper">
    <%
        starClass = "icon-star";
        photoClass = "photo";
        upperStarClass = "starIconWrapper upperStar"; 
        if (catOrDog != undefined){ 
          catOrDog == "1" ? type = "cats_thumb" : type = "dogs_thumb";
          if(main == "1"){
               starClass = "icon-star flaming";
               photoClass = "photo mainImg";
               upperStarClass = "starIconWrapper upperStar showStar";
          }%>
        <img src="frontEnd/app/img/pets/<%= type %>/<%= image %>" class="photo <%=photoClass%>" alt="Любимец">
        <% } else { %>
            <img src="<%= imgSrc %>" class="<%=photoClass%>" alt="Здесь может быть фото Вашего любимца">
            <% } %>
                <div class="overlay">
                    <div class="closeIconWrapper imgDelete">
                        <i class="icon-cancel" aria-hidden="true "></i>
                    </div>

                    <div class="starIconWrapper">
                        <i class="<%=starClass%>" aria-hidden="true"></i>
                    </div>
                </div>
                <div class="starIconWrapper upperStar <%=upperStarClass%>">
                    <i class="<%=starClass%>" aria-hidden="true"></i>
                </div>
</div>