<form class="petsForm searchForm" onsubmit="return false">
    <fieldset class="formSection petsForm_BrownDecor">
        <legend class="formSection__name">Какая у Вас цель?</legend>
        <div class="formItem formItem_formItemCon">
            <input id="searchForm__byPets" class="formItem__input petsForm_BrownDecor" type="checkbox" hidden />
            <label class="formItem__label petsForm_BrownDecor" for="searchForm__byPets">Купить любимца</label>
            <input id="searchForm__love" class="formItem__input petsForm_BrownDecor" type="checkbox" hidden />
            <label class="formItem__label petsForm_BrownDecor" for="searchForm__love">Ищу пару(вязка)</label>
            <input id="searchForm__getFree" class="formItem__input petsForm_BrownDecor" type="checkbox" hidden />
            <label class="formItem__label petsForm_BrownDecor" for="searchForm__getFree">Взять бесплатно</label>
            <input id="searchForm__rate" class="formItem__input petsForm_BrownDecor" type="checkbox" checked hidden />
            <label class="formItem__label petsForm_BrownDecor" for="searchForm__rate">Рейтинг</label>
        </div>
    </fieldset>
    <fieldset class="formSection petsForm_BrownDecor">
        <legend class="formSection__name">Какого любимца ищите?</legend>
        <div class="formItem formItem_formItemCon">
            <input id="searchForm__cat" class="formItem__input petsForm_BrownDecor" name="catsDogs" type="radio" hidden />
            <label class="formItem__label petsForm_BrownDecor" for="searchForm__cat">Кошку</label>
            <input id="searchForm__dog" class="formItem__input petsForm_BrownDecor" name="catsDogs" type="radio" hidden />
            <label class="formItem__label petsForm_BrownDecor" for="searchForm__dog">Собаку</label>
            <input id="searchForm__boy" class="formItem__input petsForm_BrownDecor" name="girlBoy" type="radio" hidden />
            <label class="formItem__label petsForm_BrownDecor" for="searchForm__boy">Мальчика</label>
            <input id="searchForm__girl" class="formItem__input petsForm_BrownDecor" name="girlBoy" type="radio" hidden />
            <label class="formItem__label petsForm_BrownDecor" for="searchForm__girl">Девочку</label>
        </div>
    </fieldset>
    <fieldset class="formSection petsForm_BrownDecor">
        <legend class="formSection__name">Какая порода и где проживает?</legend>
        <div class="formItem  formItem_getDataFromFile">
            <input id="petsForm__breed" class="formItem__input petsForm_BrownDecor formItem__input_getDataFromFile" data-type="breed" type="text" autocomplete="off" value="">
            <label class="formItem__label petsForm_BrownDecor" for="petsForm__breed"> Порода: </label>
            <div class="getDataFromFileListParent"></div>
        </div>
        <div class="formItem  formItem_getDataFromFile">
            <input id="petsForm__city" class="formItem__input petsForm_BrownDecor formItem__input_getDataFromFile" data-type="city" type="text" autocomplete="off" value="">
            <label class="formItem__label petsForm_BrownDecor" for="petsForm__city"> Ваш город: </label>
            <div class="getDataFromFileListParent"></div>
        </div>
    </fieldset>
    <div id="search_Form_submit" class="petsForm_BrownDecor mainMenuButton"> Найти любимцев</div>
</form>