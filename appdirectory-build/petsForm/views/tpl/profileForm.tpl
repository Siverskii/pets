<form class="petsForm" onsubmit="return false">
    <fieldset class="formSection">
        <div class="formItem">
            <input id="petsForm__name" class="formItem__input petsForm_BrownDecor" type="text" autocomplete="off" value="<%=name%>">
            <label class="formItem__label petsForm_BrownDecor" for="petsForm__name">Ваше имя:</label>
        </div>
        <div class="formItem formItem_getDataFromFile">
            <input id="petsForm__city" class="formItem__input petsForm_BrownDecor formItem__input_getDataFromFile" data-type="city" type="text" autocomplete="off" value="<%=city%>">
            <label class="formItem__label petsForm_BrownDecor" for="petsForm__city"> Ваш город: </label>
            <div class="getDataFromFileListParent"></div>
        </div>
    </fieldset>
    <fieldset class="formSection">
        <div class="formItem">
            <input id="petsForm__phone" class="formItem__input petsForm_BrownDecor" type="tel" autocomplete="off" value="<%=phone%>">
            <label class="formItem__label petsForm_BrownDecor" for="petsForm__phone"> Телефон: </label>
        </div>
        <div class="formItem">
            <input id="petsForm__email" class="formItem__input petsForm_BrownDecor" type="email" autocomplete="off" value="<%=email%>">
            <label class="formItem__label petsForm_BrownDecor" for="petsForm__email">Почта:</label>
        </div>
    </fieldset>
    <fieldset class="formSection">
        <div class="formItem">
            <input id="petsForm__password" class="formItem__input petsForm_BrownDecor" type="password" autocomplete="off" placeholder="Новый пароль" value="">
            <label class="formItem__label petsForm_BrownDecor" for="petsForm__password">Пароль:</label>
        </div>
        <div class="formItem">
            <input id="petsForm__passwordConfirm" class="formItem__input petsForm_BrownDecor" type="password" placeholder="Повторите пароль" autocomplete="off" value="">
            <label class="formItem__label petsForm_BrownDecor" for="petsForm__passwordConfirm">Пароль:</label>
        </div>
    </fieldset>

    <fieldset class="formSection petsForm_BrownDecor">
        <legend class="formSection__name">Желательный способ связи:</legend>
        <div class="formItem formItem_formItemCon">
            <input id="petsForm__phoneCon" class="formItem__input petsForm_BrownDecor" type="checkbox" <%=(function(){if(phoneEn==="1" ) return "checked"}()) %> hidden />
            <label class="formItem__label petsForm_BrownDecor" for="petsForm__phoneCon">Телефон</label>
            <input id="petsForm__emailCon" class="formItem__input petsForm_BrownDecor" type="checkbox" <%=(function(){if(emailEn==="1" ) return "checked"}()) %> hidden />
            <label class="formItem__label petsForm_BrownDecor" for="petsForm__emailCon">Почта</label>
        </div>
    </fieldset>
    <div class="profFormBtnWrapper">
        <div id="profileFormSubmit" class="addFormButton mainMenuButton"> Сохранить </div>
    </div>
</form>