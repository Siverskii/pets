/*
 * RequireJS underscore-tpl
 * Loads and precompile underscore templates
 * https://github.com/dciccale/requirejs-underscore-tpl
 *
 * Dependencies:
 * Underscore.js: https://underscorejs.org
 * RequireJS text plugin: https://github.com/requirejs/text
 *
 * Copyright (c) 2013 Denis Ciccale (@tdecs)
 * Licensed under the MIT license.
 * https://github.com/dciccale/requirejs-underscore-tpl/blob/master/LICENSE-MIT
 */

define(["underscore","text"],function(e,n){"use strict";var t={},r={version:"0.1.0",load:function(r,o,u,i){var c=function(n){e.extend(e.templateSettings,i.underscoreTemplateSettings||{}),n=e.template(n),n=i.isBuild?t[r]=n.source:new Function("obj","return "+n.source)(),u(n)};n.load(r,o,c,i)},write:function(e,n,r){t.hasOwnProperty(n)&&r('define("'+e+"!"+n+'", ["underscore"], function(_) { return '+t[n]+";});\n")}};return r});