'use strict';
define(["app", "marionette", "tpl!common/popUp/popUpView.tpl"], function (App, Marionette, popUpTpl) {
    var popUp = Marionette.View.extend({
        className: "popUpWindow",
        template: popUpTpl,
        events: {
            "click .closeIconWrapper": "popUpClose",
            "click .reg_btn": "popUpContinue"
        },

        popUpClose: function () {
            App.trigger("popUp:destroy_btn");

        },
        popUpContinue: function () {
            var button = $.trim($(".reg_btn").text());
            if (button == "Продолжить") {
                this.popUpClose();
                App.trigger("mainMenu:pets_btn");
            } else if (button == "Ввести новые данные") {
                App.trigger("sideBar:reg_btn");
            } else if (button == "Повторить попытку") {
                App.trigger("sideBar:aut_btn");
            } else if (button == "ОК" || button == "Ввести заново") {
                this.popUpClose();
            } else if (button == "Зарегистрироваться") {
                var email = $("#formEmail").val(),
                    password = $("#formPassword").val();
                if ((email == "") || (password == "")) {
                    return;
                }
                App.trigger("popUp:registration", {
                    email: $("#formEmail").val(),
                    password: $("#formPassword").val()
                });
            } else if (button == "Войти") {
                var email = $("#formEmail").val(),
                    password = $("#formPassword").val();
                if ((email == "") || (password == "")) {
                    return;
                }
                App.trigger("popUp:autorization", {
                    email: $("#formEmail").val(),
                    password: $("#formPassword").val()
                });
            } else if (button == "Новый поиск") {
                this.popUpClose();
                App.trigger("sideBar:findPets_btn");
            }
        }

    });
    return popUp;
});