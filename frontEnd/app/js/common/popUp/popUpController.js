'use strict';
define(["app",
        "common/popUp/popUpView"],
    function (App, popUpView) {
        function PopUpController() {
            var popUpRegion = App.getView().getRegion('popUpRegion');


            popUpRegion.on("show", function () {
                $(this.el).css('zIndex', 1);
            });
            popUpRegion.on("empty", function () {
                $(this.el).css('zIndex', -1);
            });

            App.on("popUp:destroy_btn", function () {
                var popUp = App.getView().getChildView('popUpRegion');
                if (popUp) {
                    App.getView().getChildView('popUpRegion').$el.fadeOut(300,
                        function () {
                            popUpRegion.empty();
                        });
                }
            });

            this.showFormPopUp = function (regAut) {
                var view;
                if (regAut == "reg") {
                    view = new popUpView({
                        model: new Backbone.Model({
                            popUptype: "formPopUp",
                            title: "Регистрация в Любимцах",
                            button: "Зарегистрироваться",
                            rememberPassword: false
                        })
                    });
                } else if (regAut == "aut") {
                    view = new popUpView({
                        model: new Backbone.Model({
                            popUptype: "formPopUp",
                            title: "Добро пожаловать!",
                            button: "Войти",
                            rememberPassword: true
                        })
                    });
                }

                popUpRegion.show(view);
            };
            this.showSuccessPopUp = function (popUpData) {
                var view = new popUpView({
                    model: new Backbone.Model({
                        popUptype: "successPopUp",
                        title: popUpData.title,
                        body: popUpData.body,
                        button: popUpData.button
                    })
                });
                popUpRegion.show(view);
            };
            this.showErrorPopUp = function (popUpData) {
                var view = new popUpView({
                    model: new Backbone.Model({
                        popUptype: "errorPopUp",
                        title: popUpData.title,
                        body: popUpData.body,
                        button: popUpData.button,
                    })
                });
                popUpRegion.show(view);
            };


        }
        return PopUpController;
    });