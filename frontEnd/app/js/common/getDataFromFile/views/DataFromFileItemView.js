'use strict';
define(["marionette",
       "common/getDataFromFile/dataStorage/model"],
    function (Marionette, model) {
        var breedItemView = Marionette.View.extend({
            template: _.template("<%= data %>"),
            className: "getDataFromFileList__item",
            model: model,
        });
        return breedItemView;
    });