'use strict';
define(["app", "marionette",
        "common/getDataFromFile/views/DataFromFileItemView",
       "common/getDataFromFile/views/DataFromFileEmptyView"],
    function (App, Marionette, DataFromFileItemView, DataFromFileEmptyView) {
        var collectionView = Marionette.CollectionView.extend({
            tagName: 'ul',
            className: "getDataFromFileList",
            childView: DataFromFileItemView,
            emptyView: DataFromFileEmptyView
        });
        return collectionView;
    });