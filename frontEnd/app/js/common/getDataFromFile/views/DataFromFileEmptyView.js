'use strict';
define(["marionette"],
    function (Marionette) {
        var emptyView = Marionette.View.extend({
            template: _.template("Ничего не найдено, попробуйте ввести другое значение."),
            className: "getDataFromFileList__item",
        });
        return emptyView;
    });