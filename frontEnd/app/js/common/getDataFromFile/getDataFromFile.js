'use strict';
define(["app", "common/getDataFromFile/views/DataFromFileCollectionView", "common/getDataFromFile/dataStorage/collection"],
    function (App, DataFromFileCollectionView, DataFromFileCollection) {
        function getDataFromFile() {
            var DataFromFileView;

            App.on("app:dataFromFileHandler", function (event) {
                var target = $(event.target),
                    currentTarget = $(event.currentTarget),
                    fileType = target.data("type"),
                    eventType = event.type;
                if (fileType == "breed") {
                    if (event.catsOrDogs) {
                        fileType = "catsBreed";
                    } else fileType = "dogsBreed";
                }
                if (fileType && eventType == "click" && target.attr("class") !== "getDataFromFileList__item") {
                    showDataFromFile(fileType, currentTarget);
                } else if (eventType == "keyup") {
                    showFilteredDataFromFile(target.val(), currentTarget);
                } else if (eventType == "click" && target.attr("class") == "getDataFromFileList__item") {
                    currentTarget.find(".formItem__input_getDataFromFile").val($.trim(target.text()));
                }
            });

            App.on("closeDataFromFileList", function () {
                var listContainer = $(".getDataFromFileListParent");
                if (listContainer.children().length > 0) {
                    listContainer.html("");
                }
            });

            var showDataFromFile = function (fileType, currentTarget) {
                var collection = new DataFromFileCollection();
                collection.fetch({
                    data: {
                        file: fileType
                    },
                    success: function (data) {
                        DataFromFileView = new DataFromFileCollectionView({
                            collection: data
                        });
                        currentTarget.find(".getDataFromFileListParent").html(DataFromFileView.render().el);
                    },
                    error: function () {
                        App.trigger("showConnectionErrorPopUp");
                    }
                });
            }

            var showFilteredDataFromFile = function (filter, currentTarget) {
                var filterCriterion = filter.toUpperCase();
                var newFilter = function (child, index, collection) {
                    var data = $.trim(child.get('data')).toUpperCase().split(" ");
                    var reg = new RegExp("^" + filterCriterion);
                    return reg.test(data[0]) || reg.test(data[1]);
                };
                DataFromFileView.setFilter(newFilter, {
                    preventRender: true
                });
                currentTarget.find(".getDataFromFileListParent").html(DataFromFileView.render().el);
            }

        }
        return _.extend(new getDataFromFile(), Backbone.Events);
    });