 'use strict';
 define(['backbone'], function (Backbone) {
     var dataFromFile = Backbone.Model.extend({
         defaults: {
             id: undefined,
             data: undefined
         },
     });
     return dataFromFile;
 });