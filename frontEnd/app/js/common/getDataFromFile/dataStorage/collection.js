'use strict';
define(['backbone',
    'common/getDataFromFile/dataStorage/model'], function (Backbone, breedModel) {
    var collection = Backbone.Collection.extend({
        url: '../index.php/getDataFromFile',
        model: breedModel
    });

    return collection;
});