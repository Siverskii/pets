'use strict';
define(["app", "marionette", "tpl!common/waitingIndicator/waitingIndicator.tpl"], function (App, Marionette, WITpl) {
    var waitIndicator = Marionette.View.extend({
        initialize: function () {
            App.getView().getRegion('waitIndRegion').on("before:show", function () {
                App.getView().getRegion('popUpRegion').empty();
                $(this.el).css('zIndex', 1);
            });
            App.getView().getRegion('waitIndRegion').on("empty", function () {
                $(this.el).css('zIndex', -1);
            });
        },
        className: "cssload-loader",
        template: WITpl,
    });
    return waitIndicator;
});