'use strict';
define(["app",
        "users/userModel",
        "common/popUp/popUpController",
        "jqCookie"],
    function (App, User, PopUp, Cookie) {
        function UserController() {
            App.on("popUp:registration", function (userInfo) {
                new User({
                    email: userInfo.email,
                    password: userInfo.password
                }).save(null, {
                    success: function (model, response, options) {
                        App.trigger("stopWaitingIndicator");
                        new PopUp().showSuccessPopUp({
                            title: "Ваши данные успешно сохранены!",
                            body: response,
                            button: "Продолжить"
                        });
                    },
                    error: function (model, response, options) {
                        App.trigger("stopWaitingIndicator");
                        new PopUp().showErrorPopUp({
                            title: "Ошибка регистрации!",
                            body: $.parseJSON(response.responseText),
                            button: "Ввести новые данные"
                        });
                    }
                });
                App.trigger("startWaitingIndicator");
            });

            App.on("popUp:autorization", function (unidentifiedUser) {
                App.trigger("startWaitingIndicator");
                var ajax = $.ajax({
                    method: "POST",
                    url: "/index.php/userLogIn",
                    data: {
                        email: unidentifiedUser.email,
                        password: unidentifiedUser.password
                    }
                });
                ajax.done(function (msg) {
                    App.trigger("stopWaitingIndicator");
                    App.setActiveBtn("pets_btn");
                    App.trigger("mainMenu:pets_btn");
                });
                ajax.fail(function (msg) {
                    App.trigger("stopWaitingIndicator");
                    new PopUp().showErrorPopUp({
                        title: "Ошибка авторизации!",
                        body: $.parseJSON(msg.responseText),
                        button: "Повторить попытку"
                    });
                });
            });
            App.on("sideBar:userOut_btn", function (unidentifiedUser) {
                Cookie('petsaut', null);
                var ajax = $.ajax({
                    method: "GET",
                    url: "/index.php/userLogOut"
                });
                ajax.done(function (msg) {
                    if (msg !== "ulg_ok") {
                        App.trigger("showConnectionErrorPopUp");
                    } else {
                        App.setActiveBtn("pets_btn");
                        App.trigger("mainMenu:pets_btn");
                    }
                });
                ajax.fail(function (msg) {
                    App.trigger("showConnectionErrorPopUp");
                });
            });
        }
        return new UserController();
    });