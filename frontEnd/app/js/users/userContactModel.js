'use strict';
define(["backbone"], function (Backbone) {
    var userContactModel = Backbone.Model.extend({
        urlRoot: '../index.php/getUserContact',
        defaults: {
            email: undefined,
            phone: undefined,
            emailEn: undefined,
            phoneEn: undefined,
            name: undefined,
            city: undefined
        }
    });
    return userContactModel;
});