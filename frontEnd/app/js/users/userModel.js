'use strict';
define(["backbone"], function (Backbone) {
    var userModel = Backbone.Model.extend({
        urlRoot: '../index.php/petsUser',
        defaults: {
            id: undefined,
            email: undefined,
            password: undefined,
            phone: undefined,
            emailEn: undefined,
            phoneEn: undefined,
            name: undefined,
            city: undefined
        }
    });
    return userModel;
});