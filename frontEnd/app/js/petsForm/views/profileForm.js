'use strict';
define(["app", "marionette",
       "tpl!petsForm/views/tpl/profileForm.tpl"],
    function (App, Marionette, mainTpl) {
        var profileView = Marionette.View.extend({
            template: mainTpl,
            events: {
                "click  .formItem_getDataFromFile": "dataFromFileHandler",
                "keyup  .formItem_getDataFromFile": "dataFromFileHandler",
                "click #profileFormSubmit": "profileFormSubmit"
            },

            dataFromFileHandler: function (event) {
                App.trigger("app:dataFromFileHandler", event);
            },

            profileFormSubmit: function (event) {
                var password = $("#petsForm__password"),
                    passwordConfirm = $("#petsForm__passwordConfirm"),
                    email = $("#petsForm__email").val(),
                    name = $("#petsForm__name").val(),
                    city = $("#petsForm__city").val(),
                    phone = $("#petsForm__phone").val(),
                    phoneCon = $("#petsForm__phoneCon").prop("checked"),
                    emailCon = $("#petsForm__emailCon").prop("checked"),
                    errors = false;

                if (password || passwordConfirm) {
                    if (password.val() !== passwordConfirm.val()) {
                        password.val("");
                        passwordConfirm.val("");
                        errors = "Указанные Вами пароли не совпадают."
                    }
                }
                if (!email) {
                    errors ? errors += "<br>Укажите Вашу электронную почту." : errors = "Укажите Вашу электронную почту."
                }
                if (phoneCon) {
                    if (!phone) {
                        errors ? errors += "<br>Вы указали желаемый способ связи - телефон, без указания телефона." : errors = "Вы указали желаемый способ связи - телефон, без указания телефона."
                    } else if (phone.match(/[a-zA-Zа-яА-Я]/)) {
                        errors ? errors += "<br>Номер телефона не должен содеражать буквы." : errors = "Номер телефона не должен содеражать буквы."
                    }
                }
                if ($.trim(email) != this.model.get("email")) {
                    if (!password.val()) {
                        errors ? errors += "<br>Для смены почты, укажите действующий пароль." : errors = "Для смены почты, укажите действующий пароль."
                    }
                }
                if (errors) {
                    App.trigger("profileForm:error", errors);
                } else {
                    App.trigger("profileForm:validOk", {
                        id: true,
                        email: email,
                        name: name,
                        city: city,
                        phone: phone,
                        phoneCon: phoneCon,
                        emailCon: emailCon,
                        password: password.val(),
                        passwordConfirm: passwordConfirm.val()
                    });
                }

            }
        });
        return profileView;
    });