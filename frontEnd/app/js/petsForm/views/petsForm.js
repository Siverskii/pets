'use strict';
define(["app", "marionette",
        "tpl!petsForm/views/tpl/petsForm.tpl"],
    function (App, Marionette, petsFormTpl) {
        var petsAddForm = Marionette.View.extend({
            template: petsFormTpl,
            events: {
                "change #imgUpload": "addImgToPreview",
                "click #imgUpload": "chequeQuantity",
                "click .thumb": "deleteOrSetMain",
                "click  .formItem_getDataFromFile": "dataFromFileHandler",
                "keyup  .formItem_getDataFromFile": "dataFromFileHandler",
                "click .addFormButton": "savePets",
                "click .petDeleteBtn": "deletePets",
            },

            initialize: function () {
                this.deleteImgArray = [];
            },

            addImgToPreview: function (event) {
                var img = event.target.files,
                    allowedFileTypes = ["image/png", "image/jpeg", "image/pict"],
                    imgTruType = [];
                if (img.length <= 12) {
                    _.each(img, function (image) {
                        if (allowedFileTypes.indexOf(image.type) > -1) {
                            imgTruType.push(image);
                        }
                    });
                } else {
                    App.trigger("showErrorPopUp", {
                        title: "Ошибка!",
                        body: "Вы можете загружать не более 12 фотографий своего либимца. Чтобы добавить новые фотографии, пожалуйста, удалите старые.",
                        button: "ОК"
                    });
                }
                App.trigger("petsForm:addImg", imgTruType);
            },
            chequeQuantity: function (event) {
                if ($(".allImageAddWrapper").children().length > 11) {
                    event.preventDefault();
                    App.trigger("showErrorPopUp", {
                        title: "Ошибка!",
                        body: "В настоящее время Вы можете загружать не более 12 фотографий своего либимца. Чтобы добавить новые фотографии, пожалуйста, удалите старые.",
                        button: "ОК"
                    });
                }
            },
            deleteOrSetMain: function (event) {
                var img = $(event.currentTarget),
                    souresButtonClass = $(event.target).attr("class");
                if (souresButtonClass == "icon-cancel") {
                    if (img.parent().attr("id") == "OldimageWrapper") {
                        this.deleteImgArray.push(img.find("img").attr("src").split("/")[5]);
                    }
                    img.fadeOut(300,
                        function () {
                            img.remove();
                            img = null;
                            souresButtonClass = null;
                            App.trigger("petsForm:alignColumn");
                        });
                } else if (souresButtonClass == "icon-star") {
                    $(".addForm").find(".icon-star").removeClass("flaming");
                    $(".addForm").find(".upperStar").removeClass("showStar");
                    $(".addForm").find(".photo").removeClass("mainImg");
                    img.find(".icon-star").addClass("flaming");
                    img.find(".photo").addClass("mainImg");
                    img.find(".upperStar").addClass("showStar");
                    img = null;
                    souresButtonClass = null;
                }

            },

            savePets: function () {
                var petsBreed = $.trim($("#petsForm__breed").val()),
                    oldMainImg = undefined,
                    boast = (+($("#searchForm__boast").prop("checked"))).toString(),
                    byPets = +($("#searchForm__byPets").prop("checked")),
                    love = +($("#searchForm__love").prop("checked")),
                    getFree = +($("#searchForm__getFree").prop("checked")),
                    catsOrDogs, MF, imgeArray = [];
                if (petsBreed) {
                    if ($(".allImageAddWrapper").children().length > 0) {
                        if ($(".allImageAddWrapper").find(".flaming").length > 0) {
                            var img = $("#newImageAddWrapper").find("img");
                            _.each(img, function (image) {
                                var img = $(image);
                                imgeArray.push(img.attr("src") + "," + img.hasClass("mainImg"));
                            });
                            if ($("#searchForm__cat").prop("checked"))
                                catsOrDogs = 1;
                            else if ($("#searchForm__dog").prop("checked"))
                                catsOrDogs = 0;

                            if ($("#searchForm__boy").prop("checked"))
                                MF = 0;
                            else if ($("#searchForm__girl").prop("checked"))
                                MF = 1;


                            if ($("#OldimageWrapper").find(".mainImg").attr("src")) {
                                oldMainImg = $("#OldimageWrapper").find(".mainImg").attr("src").split("/")[5];
                            }
                            App.trigger("petsForm:savePets", {
                                breed: petsBreed,
                                img: imgeArray,
                                catsOrDogs: catsOrDogs,
                                price: $("#addForm__price").val(),
                                sellLoveGift: parseInt(boast + getFree + love + byPets, 2),
                                descr: $("#addForm__descr").val(),
                                name: $("#petsForm__name").val(),
                                MF: MF,
                                id: this.model.get("id"),
                                mainImg: oldMainImg,
                                deleteImgArray: this.deleteImgArray
                            });
                        } else {
                            App.trigger("showErrorPopUp", {
                                title: "Ошибка!",
                                body: "Выбирите главную фотографию Вашего любимца, которая будет учавствовать в рейтинге и поиске, для этого, наведите курсор мыши на фотографию и нажмите на появившуюся зведу.",
                                button: "ОК"
                            });
                        }
                    } else {
                        App.trigger("showErrorPopUp", {
                            title: "Ошибка!",
                            body: "Ваш любимец остался без фотографий!",
                            button: "ОК"
                        });
                    }
                } else {
                    App.trigger("showErrorPopUp", {
                        title: "Ошибка!",
                        body: "Ваш любимец остался без породы, пожалуйста, укажите ее!",
                        button: "ОК"
                    });
                }
            },
            deletePets: function () {
                App.trigger("petsForm:deletePets", this.model);
            },
            dataFromFileHandler: function (event) {
                var catsOrDogs = false;
                if ($("#searchForm__cat").prop("checked"))
                    catsOrDogs = true;
                event.catsOrDogs = catsOrDogs;
                App.trigger("app:dataFromFileHandler", event);
            },
        });
        return petsAddForm;
    });