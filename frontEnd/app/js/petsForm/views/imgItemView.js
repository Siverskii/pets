'use strict';
define(["marionette",
        "tpl!petsForm/views/tpl/imgItemView.tpl",
       "petsForm/pets/model/imgModel"],
    function (Marionette, ImgItemTpl, imgModel) {
        var ImgItemView = Marionette.View.extend({
            template: ImgItemTpl,
            model: imgModel,
            className: "col-lg-3 col-xs-6 thumb",

            initialize: function (options) {
                if (options.catOrDog !== undefined) {
                    this.model.set({
                        catOrDog: options.catOrDog
                    });
                }
            }
        });
        return ImgItemView;
    });