'use strict';
define(['app', "marionette", "tpl!petsForm/views/tpl/searchForm.tpl"], function (App, Marionette, searchFormHeader) {
    var searchFormHeader = Marionette.View.extend({
        template: searchFormHeader,
        events: {
            "click  .formItem_getDataFromFile": "dataFromFileHandler",
            "keyup  .formItem_getDataFromFile": "dataFromFileHandler",
            "click #search_Form_submit": "destroyView"
        },

        dataFromFileHandler: function (event) {
            var catsOrDogs = false;
            if ($("#searchForm__cat").prop("checked"))
                catsOrDogs = true;
            event.catsOrDogs = catsOrDogs;
            App.trigger("app:dataFromFileHandler", event);
        },
        destroyView: function () {
            var view = this;
            view.$el.fadeOut(300, function () {
                var formData = {
                    byPets: $("#searchForm__byPets").prop("checked"),
                    love: $("#searchForm__love").prop("checked"),
                    getFree: $("#searchForm__getFree").prop("checked"),
                    rate: $("#searchForm__rate").prop("checked"),
                    cat: $("#searchForm__cat").prop("checked"),
                    dog: $("#searchForm__dog").prop("checked"),
                    boy: $("#searchForm__boy").prop("checked"),
                    girl: $("#searchForm__girl").prop("checked"),
                    breed: $.trim($("#petsForm__breed").val()),
                    city: $.trim($("#petsForm__city").val()),
                }
                App.trigger("searchForm:submit", formData);
                view.destroy();
            });
        }
    });
    return searchFormHeader;
});