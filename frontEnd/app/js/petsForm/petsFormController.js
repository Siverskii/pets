'use strict';
define(["app",
        "marionette",
        "petsForm/views/petsForm",
        "petsForm/pets/collection/imgCollection",
        "petsForm/pets/model/imgModel",
        "pets/petsData/petsModel",
        "petsForm/views/profileForm",
        "users/userModel",
        "petsForm/views/searchForm",
        "pets/petsData/petsCollection",
        "pets/header/header",
       "petsForm/views/imgItemView",
       "common/getDataFromFile/getDataFromFile"],
    function (App, Marionette, petsForm, imgCollection, imgModel, petsModel, profileForm, userModel, searchForm, petsCollection, Header, imgItemView) {
        function petsAddFormController() {
            var DataFromFileView = false;
            this.showAddForm = function (model) {
                if (!model)
                    var model = new petsModel();
                App.getView().getRegion('mainRegion').show(new petsForm({
                    model: model
                }));
                var images = model.get("images");
                if (images) {
                    var ImageCollection = new imgCollection();
                    _.each(images, function (img) {
                        ImageCollection.add(new imgModel({
                            image: img.image,
                            likes: img.likes,
                            main: img.main,
                            catOrDog: model.get("catOrDog")
                        }));
                    });
                    var ImgCollect = Marionette.CollectionView.extend({
                        el: "#OldimageWrapper",
                        collection: ImageCollection,
                        childView: imgItemView
                    });
                    var imgCollect = new ImgCollect();
                    imgCollect.on("dom:refresh", function () {
                        App.trigger("petsForm:alignColumn");
                    });
                    imgCollect.render();
                }
            }
            App.on("petsForm:addImg", function (img) {
                var ImageCollection = new imgCollection();
                _.each(img, function (image) {
                    var reader = new FileReader();
                    reader.addEventListener("load", function () {
                        ImageCollection.add(new imgModel({
                            imgSrc: this.result
                        }));
                    }, false);
                    reader.readAsDataURL(image);
                });

                var ImgCollect = Marionette.CollectionView.extend({
                    el: "#newImageAddWrapper",
                    collection: ImageCollection,
                    childView: imgItemView
                });
                var imgCollect = new ImgCollect();
                imgCollect.on("dom:refresh", function () {
                   App.trigger("petsForm:alignColumn");
                });
                imgCollect.render();
            });

            App.on("petsForm:savePets", function (petsDetails) {
                App.trigger("startWaitingIndicator");
                new petsModel(petsDetails).save(null, {
                    success: function () {
                        App.trigger("stopWaitingIndicator");
                        App.trigger("sideBar:myPets_btn");
                        App.trigger("petsForm:alignColumn");
                    },
                    error: function () {
                        App.trigger("stopWaitingIndicator");
                        App.trigger("showConnectionErrorPopUp");
                    }
                });
            });
            App.on("petsForm:deletePets", function (model) {
                App.trigger("startWaitingIndicator");

                model.destroy({
                    success: function () {
                        App.trigger("stopWaitingIndicator");
                        App.trigger("sideBar:myPets_btn");
                        App.trigger("petsForm:alignColumn");
                    },
                    error: function () {
                        App.trigger("stopWaitingIndicator");
                        App.trigger("showConnectionErrorPopUp");
                    }
                });
            });
            App.on("petsForm:alignColumn", function () {
                    var timerId = setInterval(function () {
                        var maxHeight = parseInt($(App.getView().getRegion('mainRegion').el).css("height").split('px')[0]) + 0 + "px";
                        $(App.getView().getRegion('sideBarRegion').el).css("height", maxHeight);
                    }, 10);
                    setTimeout(function () {
                        clearInterval(timerId);
                    }, 500);
            });

            this.showProfile = function () {
                if (!App.getView().getRegion("sideBarRegion").hasView()) {
                    App.getView().render();
                }
                App.trigger("petsForm:alignColumn");
                new userModel().fetch({
                    success: function (userInfo) {
                        App.getView().getRegion('headerRegion').show(new Header({
                            model: App.getUserValidModel()
                        }));
                        App.getView().getRegion('mainRegion').show(new profileForm({
                            model: userInfo
                        }));
                    },
                    error: function () {
                        App.trigger("showConnectionErrorPopUp");
                    }
                });
            }

            App.on("profileForm:error", function (errors) {
                App.trigger("showErrorPopUp", {
                    title: "Ошибка формы профиля",
                    body: errors,
                    button: "Ввести заново"
                });

            });

            App.on("profileForm:validOk", function (userInfo) {
                new userModel(userInfo).save(null, {
                    success: function () {
                        App.trigger("showSuccessPopUp", {
                            title: "Изменение данных профиля",
                            body: "Выши данные успешно сохранены!",
                            button: "ОК"
                        });
                    },
                    error: function () {
                        App.trigger("showConnectionErrorPopUp");
                    }
                });
            });



            this.getSerchForm = function () {
                return new searchForm();
            }
            App.on("searchForm:submit", function (formData) {
                App.trigger("startWaitingIndicator");
                new petsCollection().fetch({
                    data: formData,
                    success: function (collection, resp) {
                        App.trigger("stopWaitingIndicator");
                        App.trigger("searchForm:gotCollection", collection);
                    },
                    error: function (model, resp) {
                        App.trigger("stopWaitingIndicator");
                        if (resp.status == 404) {
                            App.trigger("stopWaitingIndicator");
                            App.trigger("showErrorPopUp", {
                                title: "Ошибка!",
                                body: "Любимцев с такими параметрами нет, попробуйте изменить поиск",
                                button: "Новый поиск"
                            });
                            return;
                        }
                        App.trigger("stopWaitingIndicator");
                        App.trigger("showConnectionErrorPopUp");
                    }
                });
            });
        }

        return new petsAddFormController();
    });