 'use strict';
 define(["backbone"], function (Backbone) {
     var imgModel = Backbone.Model.extend({
         defaults: {
             id: undefined,
             imgSrc: undefined,
             image: undefined,
             likes: undefined,
             main: undefined,
             catOrDog: undefined
         },
     });
     return imgModel;
 });