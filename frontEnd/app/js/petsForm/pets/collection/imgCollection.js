'use strict';
define(['backbone', 'petsForm/pets/model/imgModel'], function (Backbone, imgModel) {
    var Collection = Backbone.Collection.extend({
        url: "../index.php/getAllPetImage",
        model: imgModel
    });

    return Collection;
});