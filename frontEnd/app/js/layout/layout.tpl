<div class="col-md-3 hidden-xs hidden-sm padding_0px_5px">
    <div class="layoutRegionWrapper">
        <div id="sideBarHeader" class="menuSideBar"></div>
        <div id="sideBarRegion"></div>
        <div id="sideBarFooter" class="menuSideBar"></div>
    </div>
</div>
<div class="col-md-9 col-xs-12 padding_0px_5px">
    <div class="layoutRegionWrapper">
        <div id="headerRegion"></div>
        <div id="mainRegion"></div>
        <div id="footerRegion"></div>
        <div id="popUpRegion"></div>
        <div id="waitIndRegion">
        </div>
    </div>
</div>