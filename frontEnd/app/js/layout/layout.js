'use strict';
define(['app', "marionette", "tpl!layout/layout.tpl", "pets/header/header", "pets/footer/footer", "pets/sideBar/sideBar", "pets/sideBar/sideBarHeader", "users/userModel"],
    function (App, Marionette, layoutTpl, Header, Footer, SideBar, sideBarHeader, userModel) {
        var layout = Marionette.View.extend({
            template: layoutTpl,
            regions: {
                sideBarHeader: "#sideBarHeader",
                sideBarRegion: "#sideBarRegion",
                headerRegion: "#headerRegion",
                mainRegion: "#mainRegion",
                footerRegion: "#footerRegion",
                popUpRegion: "#popUpRegion",
                waitIndRegion: "#waitIndRegion"
            },

            initialize: function () {
                this.on("render", function () {
                    var userValid = App.getUserValidModel();

                    this.getRegion('headerRegion').show(new Header({
                        model: userValid
                    }));
                    this.getRegion('footerRegion').show(new Footer({
                        model: userValid
                    }));
                    this.getRegion('sideBarRegion').show(new SideBar({
                        model: userValid
                    }));

                    var sideBarHeaderRegion = this.getRegion('sideBarHeader');
                    if (userValid.get("userValidate")) {

                        new userModel().fetch({
                            success: function (userInfo) {
                                sideBarHeaderRegion.show(new sideBarHeader({
                                    model: userInfo
                                }));
                            },
                            error: function () {
                                App.trigger("showConnectionErrorPopUp");
                            }
                        });
                    }
                    App.trigger("popUp:destroy_btn");
                });
            },

            events: {
                "mousedown #popUpRegion": "setCurentPosPopUpWindow",
                "mousemove #mainRegion, #popUpRegion, #headerRegion, #footerRegion": "movePopUpWindow",
                "mouseup #popUpRegion": "moveEndPopUpWindow",
                "click .layoutRegionWrapper": "closeDataFromFileList"
            },


            setCurentPosPopUpWindow: function (event) {
                var popUpWindow = $(event.target);
                if (popUpWindow.attr("class").match("popUpHeader") || popUpWindow.attr("class").match("popUpTitle")) {
                    this.CurrentpositionX = Math.round(popUpWindow.parent().offset().left);
                    this.CurrentpositionY = Math.round(popUpWindow.parent().offset().top);
                    this.mouseCurntPositionX = event.pageX;
                    this.mouseCurntPositionY = event.pageY;
                    this.mouseShiftX = event.pageX - this.CurrentpositionX;
                    this.mouseShiftY = event.pageY - this.CurrentpositionY;
                    this.moveEn = true;
                }
            },
            movePopUpWindow: function (event) {
                if (this.moveEn) {
                    var moveX = this.CurrentpositionX + (event.pageX - this.mouseCurntPositionX),
                        moveY = this.CurrentpositionY + (event.pageY - this.mouseCurntPositionY);
                    $(".popUpWindow").offset({
                        left: moveX,
                        top: moveY
                    });
                }
            },
            moveEndPopUpWindow: function () {
                this.moveEn = false;
            },
            closeDataFromFileList: function (event) {
                App.trigger("closeDataFromFileList");
            },
        });
        return new layout();
    });