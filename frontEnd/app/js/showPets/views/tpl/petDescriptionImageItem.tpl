<% catOrDog == "1" ? type = "cats_thumb" : type = "dogs_thumb" %>
    <div class="petsWrapper petsWrapper_showPetsDescription">
        <div class="petsImgWrapper">
            <img src="frontEnd/app/img/pets/<%= type %>/<%= image %>" alt="Любимец">
        </div>
        <div class="petsCondition petsCondition_showPetsDescription">
            <div class="petsLikes">
                <%= likes %> <i class="icon-heart" aria-hidden="true"></i>
            </div>

        </div>
    </div>