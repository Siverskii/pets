'use strict';
define(["marionette",
       "tpl!showPets/views/tpl/petDescriptionImageItem.tpl",
       "petsForm/pets/model/imgModel"],
    function (Marionette, petDescriptionItem, imgModel) {
        var ImgItemView = Marionette.View.extend({
            template: petDescriptionItem,
            model: imgModel,
            className: "col-lg-3 col-sm-4 col-xs-6 thumb2",

            initialize: function (options) {
                if (options.catOrDog !== undefined) {
                    this.model.set({
                        catOrDog: options.catOrDog
                    });
                }
            }
        });
        return ImgItemView;
    });