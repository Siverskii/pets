'use strict';
define(["app", "marionette", "tpl!showPets/views/tpl/petsView.tpl"], function (App, Marionette, mainRegionTpl) {
    var mainRegion = Marionette.View.extend({
        className: "petsMainSection",
        template: mainRegionTpl,
        events: {
            "click #like_btn": "like_btn",
            "click .mainPetsImg": "openPetsDescription",
            "click #dislike_btn": "dislike_btn",
        },

        like_btn: function () {
            App.trigger("like_btn:pressed", this.model.get("id"));
        },
        dislike_btn: function () {
            App.trigger("dislike_btn:pressed");
        },
        openPetsDescription: function () {
            App.trigger("openPetsDescription", this.model);
        }

    });
    return mainRegion;
});