'use strict';
define(['app', "marionette", "tpl!showPets/views/tpl/petDescriptionUsersContact.tpl"], function (App, Marionette, petDescriptionUsersContact) {
    var Contact = Marionette.View.extend({
        template: petDescriptionUsersContact
    });
    return Contact;
});