'use strict';
define(['app', "marionette", "tpl!showPets/views/tpl/petDescriptionItem.tpl"], function (App, Marionette, petsDescriptionItem) {
    var petDescription = Marionette.View.extend({
        template: petsDescriptionItem,
        events: {
            "click #petsDescription_showOwnerContact": "showOwnerContact",
            "click #petsDescription_closePets": "closePets"
        },

        showOwnerContact: function (event) {
            $(event.target).addClass("activeBtn");
            App.trigger("showOwnerContact", this.model.get("ownerID"));
        },
        closePets: function () {
            Backbone.history.history.back();
        }
    });
    return petDescription;
});