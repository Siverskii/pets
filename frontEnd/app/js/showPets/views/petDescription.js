'use strict';
define(['app', "marionette", "tpl!showPets/views/tpl/petDescription.tpl"], function (App, Marionette, petDescription) {
    var petDescription = Marionette.View.extend({
        template: petDescription,
        regions: {
            petDescription: "#petDescription",
            petDescriptionImages: "#petDescriptionImages",
            petDescriptionUsersContact: "#petDescriptionUsersContact"
        },
    });
    return petDescription;
});