'use strict';
define(["jqCookie",
        "app",
        "marionette",
        "backbone",
        "pets/header/header",
        "showPets/views/petsView",
        "pets/PetsData/petsCollection",
        "petsForm/pets/collection/imgCollection",
        "showPets/views/petDescription",
        "showPets/views/petDescriptionImageItem",
        "showPets/views/petDescriptionItem",
        "users/userContactModel",
        "showPets/views/petDescriptionUsersContact",
        "pets/PetsData/petsModel"],
    function (Cookie, App, Marionette, Backbone, Header, petsView,
        petsCollection, imgCollection, petDescription, petDescriptionImageItem, petDescriptionItem, userContactModel, petDescriptionUsersContact, petsModel) {
        function ShowCotroller() {
            var showCotroller = this;
            var layout = App.getView();
            this.showPets = function () {
                layout.render();
                App.trigger("petsForm:alignColumn");
                App.trigger("popUp:destroy_btn");
                fetchPetsCollection();
            }
            var fetchPetsCollection = function () {
                if (showCotroller.collection === undefined || showCotroller.collection.length == 0) {
                    App.trigger("startWaitingIndicator");
                    var petsCollect = new petsCollection();
                    petsCollect.fetch({
                        success: function (collection) {
                            showCotroller.collection = collection;
                            showPetsFromCollections(showCotroller.collection);
                        },
                        error: function () {
                            App.trigger("stopWaitingIndicator");
                            App.trigger("showConnectionErrorPopUp");
                        }
                    });
                } else {
                    showPetsFromCollections(showCotroller.collection);
                }
            }

            var showPetsFromCollections = function (collection) {
                if (showCotroller.model = collection.pop()) {
                    showCotroller.model.set("userValidate", Cookie('petsaut'));
                    layout.getRegion('headerRegion').show(new Header({
                        model: showCotroller.model
                    }));
                    var petView = new petsView({
                        model: showCotroller.model
                    });
                    petView.on("dom:refresh", function () {
                        App.trigger("stopWaitingIndicator");
                    });
                    layout.getRegion('mainRegion').show(petView);
                } else {
                    fetchPetsCollection();
                }
            }

            App.on("like_btn:pressed", function (petsId) {
                var ajax = $.ajax({
                    method: "PUT",
                    url: "/index.php/likeToImg",
                    data: {
                        id: petsId
                    }
                });
                ajax.done(function (msg) {
                    showPetsFromCollections(showCotroller.collection);

                });
                ajax.fail(function (msg) {
                    this.App.trigger("showConnectionErrorPopUp");
                });
            });
            App.on("dislike_btn:pressed", function () {
                showPetsFromCollections(showCotroller.collection);
            });

            this.showPetsDescription = function (pets) {
                if ((pets instanceof Backbone.Model)) {
                    showCotroller.description = new petDescription();
                    showCotroller.description.on("dom:refresh", function () {
                        App.trigger("petsForm:alignColumn");
                    });
                    App.trigger("startWaitingIndicator");
                    new imgCollection().fetch({
                        data: {
                            id: pets.get("id")
                        },
                        success: function (collection) {
                            App.trigger("stopWaitingIndicator");
                            var imgCollection = Marionette.CollectionView.extend({
                                className: "col-xs-10 col-xs-offset-1  userPetsCollection",
                                collection: collection,
                                childViewOptions: {
                                    catOrDog: pets.get("catOrDog")
                                },
                                childView: petDescriptionImageItem
                            });
                            var imgCollection = new imgCollection();

                            layout.getRegion('mainRegion').show(showCotroller.description);
                            showCotroller.description.getRegion("petDescriptionImages").show(imgCollection);
                            showCotroller.description.getRegion("petDescription").show(new petDescriptionItem({
                                model: pets
                            }));
                        },
                        error: function () {
                            App.trigger("stopWaitingIndicator");
                            App.trigger("showConnectionErrorPopUp");
                        }
                    });
                } else {
                    layout.render();
                    new petsModel().fetch({
                        data: {
                            petsId: pets
                        },
                        success: function (model) {
                            showCotroller.showPetsDescription(model);
                        },
                        error: function () {
                            App.trigger("stopWaitingIndicator");
                            App.trigger("showConnectionErrorPopUp");
                        }
                    });
                }

            }
            App.on("showOwnerContact", function (ownerID) {
                new userContactModel().fetch({
                    data: {
                        id: ownerID
                    },
                    success: function (model) {
                        showCotroller.description.getRegion("petDescriptionUsersContact").show(new petDescriptionUsersContact({
                            model: model
                        }));
                        App.trigger("petsForm:alignColumn");
                    },
                    error: function (model, response) {
                        if (response.responseJSON == "autFail") {
                            App.trigger("showNeedRegistrationErrorPopUp");
                        } else
                            App.trigger("showNoUsersContactErrorPopUp");
                    }
                });
            });
            App.on("petsForm:alignColumn", function () {
                    var timerId = setInterval(function () {
                        var maxHeight = parseInt($(App.getView().getRegion('mainRegion').el).css("height").split('px')[0]) + 0 + "px";
                        $(App.getView().getRegion('sideBarRegion').el).css("height", maxHeight);
                    }, 10);
                    setTimeout(function () {
                        clearInterval(timerId);
                    }, 500);
            });
        }
        return new ShowCotroller();
    });