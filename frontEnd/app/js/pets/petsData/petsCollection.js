'use strict';
define(["backbone", 'pets/petsData/petsModel'], function (Backbone, petsModel) {
    var Collection = Backbone.Collection.extend({
        url: '../index.php/petsCollection',
        model: petsModel
    });

    return Collection;
});