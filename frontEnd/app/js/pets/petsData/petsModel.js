 'use strict';
 define(["backbone"], function (Backbone) {
     var petsModel = Backbone.Model.extend({
         urlRoot: "../index.php/pets",
         defaults: {
             id: undefined,
             breed: undefined,
             images: [],
             catOrDog: undefined,
             MF: undefined,
             age: undefined,
             monthEars: undefined,
             name: undefined,
             sellLoveGift: '0',
             price: undefined,
             love: undefined,
             lovePrice: undefined,
             descr: undefined,
             boast: undefined,
             ownerID: undefined
         },
     });
     return petsModel;
 });