'use strict';
define(["app", "marionette"],
    function (App, Marionette) {
        var petsRouter = Marionette.AppRouter.extend({
            appRoutes: {
                "showPets": "showPets",
                "myPets": "showMyPets",
                "myPets/:id": "showMyPetsDescr",
                "myProfile": "showMyProfile",
                "petsSearch": "petsSearch",
                "pets/:id": "showPetsDescr",
                "about": "showAbout"
            }
        });
        var controller = {
            showPets: function () {
                require(["showPets/showController"], function (showController) {
                    showController.showPets();
                });
            },
            showPetsDescr: function (pet) {
                require(["showPets/showController"], function (showController) {
                    showController.showPetsDescription(pet);
                });
            },
            //SideBar menu handle
            showMyPets: function () {
                require(["myPets/myPetsController"], function (myPetsController) {
                    myPetsController.showMyPets();
                });
            },
            showMyPetsDescr: function (petsId) {
                require(["myPets/myPetsController"], function (myPetsController) {
                    myPetsController.showPetsDescr(petsId);
                });
            },
            showMyProfile: function () {
                require(["petsForm/petsFormController"], function (petsFormController) {
                    App.trigger("stopWaitingIndicator");
                    petsFormController.showProfile();
                });
            },
            petsSearch: function () {
                require(["searchPets/petsSearchController"], function (petsSearchController) {
                    App.trigger("stopWaitingIndicator");
                    petsSearchController.showSearchForm();
                });
            },
            showAbout: function () {
                require(["pets/footer/about", "pets/header/header"], function (about, header) {
                    App.trigger("stopWaitingIndicator");
                    App.getView().getRegion('mainRegion').show(new about());
                    App.getView().getRegion('headerRegion').show(new header({
                        model: App.getUserValidModel()
                    }));
                });

            }

        }

        //Main menu handle
        App.on("sideBar:myPets_btn", function () {
            App.navigate("myPets");
            controller.showMyPets();

        });
        App.on("sideBar:myProfile_btn", function () {
            App.navigate("myProfile");
            controller.showMyProfile();

        });
        App.on("sideBar:findPets_btn", function () {
            App.navigate("petsSearch");
            controller.petsSearch();
        });

        //Footer handle
        App.on("footer:about_btn", function () {
            App.navigate("about");
            controller.showAbout();

        });
        //Other handlers
        App.on("mainMenu:pets_btn", function () {
            App.navigate("showPets");
            controller.showPets();
        });
        App.on("showPets", function () {
            App.navigate("showPets");
            controller.showPets();
        });
        App.on("userPets:openPetsDescr", function (petsId) {
            App.navigate("myPets/" + petsId);
            controller.showMyPetsDescr(petsId);
        });
        App.on("openPetsDescription", function (pets) {
            App.navigate("pets/" + pets.get("id"));
            controller.showPetsDescr(pets);
        });

        return new petsRouter({
            controller: controller
        });
    });