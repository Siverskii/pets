'use strict';
define(['app', "marionette", "tpl!pets/footer/about.tpl"], function (App, Marionette, footerTpl) {
    var about = Marionette.View.extend({
        template: footerTpl,
    });
    return about;
});