<section class="helpSection">
    <div class="helpSectionItem">
        Добро пожаловать к любимцам!
        <br>
        <br> На нашем сайте Вы можете похвастаться фотографиями своего любимца, а если у Вас его еще нет, то найти того, кто может продать или отдать его в хорошие руки.
        <br>
        <br>Используйте кнопку "Любимцы", для возврата к главному меню сайта.
    </div>
    <div class="helpSectionItem">
        <div class="helpSectionItem__icon">
            <i class="icon-rouble"></i>
        </div>
        <div class="helpSectionItem__descr"> - Стоимость любимца, в случае его продажи или вязки.</div>
    </div>
    <div class="helpSectionItem">
        <div class="helpSectionItem__icon">
            <i class="icon-home"></i>
        </div>
        <div class="helpSectionItem__descr"> - Зеленый цвет иконки, указывает готовность владельца отдать любимца бесплатно.</div>
    </div>
    <div class="helpSectionItem">
        <div class="helpSectionItem__icon">
            <i class="icon-venus-mars"></i>
        </div>
        <div class="helpSectionItem__descr"> - Зеленый цвет иконки указывает на желание осуществить вязку.</div>
    </div>
    <div class="helpSectionItem">
        <div class="helpSectionItem__icon">
            <i class="icon-heart"></i>
        </div>
        <div class="helpSectionItem__descr"> - Количество лайков у любимца.</div>
    </div>
    <div class="helpSectionItem">
        <div class="helpSectionItem__icon">
            <i class="icon-paw"></i>
        </div>
        <div class="helpSectionItem__descr"> - Количество любимцев пользователя.</div>
    </div>
    <div class="helpSectionItem">
        <div class="helpSectionItem__icon">
            <i class="icon-star-empty"></i>
        </div>
        <div class="helpSectionItem__descr"> - Выбор фотографии для показа в поиске.</div>
    </div>
    <div class="helpSectionItem">
        По всем вопросам обращаться по адресу siverskii@yandex.ru.
    </div>



</section>