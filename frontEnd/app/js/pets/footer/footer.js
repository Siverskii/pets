'use strict';
define(['app', "marionette", "tpl!pets/footer/footer.tpl"], function (App, Marionette, footerTpl) {
    var footer = Marionette.View.extend({
        className: "horizontalPanel",
        template: footerTpl,
        events: {
            "click .mainMenuButton": "footerHandler"
        },
        footerHandler: function (event) {
            switch ($(event.target).attr("id")) {
            case "myPets_btn":
                {
                    App.setActiveBtn("myPets_btn");
                    App.trigger("sideBar:myPets_btn");
                    break;
                }

            case "myProfile_btn":
                {
                    App.setActiveBtn("myProfile_btn");
                    App.trigger("sideBar:myProfile_btn");
                    break;
                }

            case "pets_btn":
                {
                    App.setActiveBtn("pets_btn");
                    App.trigger("sideBar:pets_btn");
                    break;
                }

            case "findPets_btn":
                {
                    App.setActiveBtn("findPets_btn");
                    App.trigger("sideBar:findPets_btn");
                    break;
                }

            case "rankPets_btn":
                {
                    App.setActiveBtn("rankPets_btn");
                    App.trigger("sideBar:rankPets_btn");
                    break;
                }
            case "rg_btn":
                {
                    require(["users/userController"], function () {
                        App.setActiveBtn("reg_btn");
                        App.trigger("sideBar:reg_btn");

                    });

                    break;
                }
            case "at_btn":
                {
                    require(["users/userController"], function () {
                        App.setActiveBtn("aut_btn");
                        App.trigger("sideBar:aut_btn");

                    });
                    break;
                }
            case "about_btn":
                {
                    App.setActiveBtn("about_btn");
                    App.trigger("footer:about_btn");
                    App.trigger("petsForm:alignColumn");
                    break;
                }
            }
        }
    });
    return footer;
});