'use strict';
define(["app", "marionette",
        "tpl!pets/header/header.tpl"],
    function (App, Marionette, mainMenuTpl) {
        var mainMenu = Marionette.View.extend({
            className: "horizontalPanel",
            template: mainMenuTpl,
            events: {
                "click #pets_btn": "getHome",
                "click #userExit": "userOut",
            },

            initialize: function () {
                var header = this;
                this.on("destroy", function () {

                });
            },
            getHome: function () {
                App.setActiveBtn("pets_btn");
                App.trigger("mainMenu:pets_btn");
            },
            userOut: function () {
                require(["users/userController"], function () {
                    App.trigger("sideBar:userOut_btn");
                });


            }
        });
        return mainMenu;
    });