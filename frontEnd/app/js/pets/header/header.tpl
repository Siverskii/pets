<div class="horizontalPanel">
    <% if(userValidate == "true"){ %>

        <div id="userExit" class="mainMenuButton mainMenuButton_forHorizontalPanel">
            <i class="icon-logout"></i>
        </div>
        <% } %>
            <div class="row">
                <div class="col-lg-6 col-lg-offset-3 col-sm-8  col-sm-offset-2">
                    <div class="horizontalPanel horizontalPanel_central">
                        <div class="mainMenuButton mainMenuButton_forMainMenuCentralPanel activeBtn" id="pets_btn">Любимцы</div>
                        <ul class="dopMenu">
                            <li class="<%=(function(){if(sellLoveGift == '1' && sellLoveGift !== '2') return 'dopMenuActiv'}()) %>">
                                <%= price %> <i class="icon-rouble"></i>
                            </li>
                            <li class="<%=(function(){if(sellLoveGift === '4') return 'dopMenuActiv'}()) %>"> <i class="icon-home"></i></li>
                            <li class="<%=(function(){if(sellLoveGift === '2') return 'dopMenuActiv'}()) %>">
                                <i class="icon-venus-mars"></i>
                            </li>
                            <li id="<%=(function(){if(likes == '') return 'dopMenuOff'}()) %>">
                                <%= likes %> <i class="icon-heart" aria-hidden="true"></i>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
</div>