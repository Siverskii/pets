'use strict';
define(["app",
        "marionette",
        "tpl!pets/sideBar/sideBarHeader.tpl"],
    function (App, Marionette, mainMenuTpl) {
        var sideBarHeader = Marionette.View.extend({
            template: mainMenuTpl,

        });
        return sideBarHeader;
    });