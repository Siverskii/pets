'use strict';
define(["app",
        "marionette",
        "tpl!pets/sideBar/sideBar.tpl"],
    function (App, Marionette, mainMenuTpl) {
        var sideBar = Marionette.View.extend({
            template: mainMenuTpl,
            events: {
                "click .mainMenuButton": "menuHandler"
            },
            menuHandler: function (event) {
                switch ($(event.target).attr("id")) {
                case "myPets_btn":
                    {
                        App.setActiveBtn("myPets_btn");
                        App.trigger("sideBar:myPets_btn");
                        break;
                    }

                case "myProfile_btn":
                    {
                        App.setActiveBtn("myProfile_btn");
                        App.trigger("sideBar:myProfile_btn");
                        break;
                    }
                case "findPets_btn":
                    {
                        App.setActiveBtn("findPets_btn");
                        App.trigger("sideBar:findPets_btn");
                        break;
                    }
                case "reg_btn":
                    {
                        require(["users/userController"], function () {
                            App.setActiveBtn("reg_btn");
                            App.trigger("sideBar:reg_btn");

                        });
                        break;

                    }
                case "aut_btn":
                    {
                        require(["users/userController"], function () {
                            App.setActiveBtn("aut_btn");
                            App.trigger("sideBar:aut_btn");

                        });
                        break;
                    }
                }

            }
        });
        return sideBar;

    });