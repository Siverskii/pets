'use strict';
define(["marionette", "jqCookie", "text", "tpl"], function (Marionette, Cookie) {
    var Pets = new Marionette.Application({
        region: '#Pets',
        onStart: function () {
            require(["layout/layout", "pets/petsRouter", "common/waitingIndicator/waitingIndicator", "common/popUp/popUpController"], function (Layout, Router, WaitingIndicator, PopUp) {

                Pets.showView(Layout);

                Pets.on("stopWaitingIndicator", function () {
                    Pets.getView().getRegion('waitIndRegion').empty();
                });
                Pets.on("startWaitingIndicator", function () {
                    Pets.getView().getRegion('waitIndRegion').show(new WaitingIndicator());
                });
                Pets.trigger("startWaitingIndicator");

                Pets.on("sideBar:reg_btn", function () {
                    new PopUp().showFormPopUp("reg");
                });
                Pets.on("sideBar:aut_btn", function () {
                    new PopUp().showFormPopUp("aut");

                });
                Pets.on("showErrorPopUp", function (data) {
                    new PopUp().showErrorPopUp({
                        title: data.title,
                        body: data.body,
                        button: data.button
                    });
                });
                Pets.on("showConnectionErrorPopUp", function (data) {
                    new PopUp().showErrorPopUp({
                        title: "Ошибка!",
                        body: "Сбой соединения, пожалуйста проверьте подключение к интернету и повторите попытку",
                        button: "ОК"
                    });

                });
                Pets.on("showNoUsersContactErrorPopUp", function (data) {
                    new PopUp().showErrorPopUp({
                        title: "Ошибка!",
                        body: "Пользователь не указал контактных данных",
                        button: "ОК"
                    });

                });
                Pets.on("showNeedRegistrationErrorPopUp", function (data) {
                    new PopUp().showErrorPopUp({
                        title: "Ошибка!",
                        body: "Для просмотра контактных данных, необходимо зарегистрироваться.",
                        button: "ОК"
                    });
                });
                Pets.on("showSuccessPopUp", function (data) {
                    new PopUp().showSuccessPopUp({
                        title: data.title,
                        body: data.body,
                        button: data.button
                    });
                });
                Backbone.history.start();
                if (Backbone.history.fragment === "") {
                    Pets.trigger("showPets");
                }

            });
        }
    });


    Pets.navigate = function (route, options) {
        options || (options = {});
        Backbone.history.navigate(route, options);
    };

    Pets.setActiveBtn = function (btn) {
        var btnArray = ["myPets_btn", "myProfile_btn", "pets_btn", "findPets_btn", "rankPets_btn",
                                   "optoins_btn", "about_btn", "rules_btn", "contacts_btn", "reg_btn", "aut_btn"];
        _.each(btnArray, function (btns) {
            if (btn !== btns)
                $("#" + btns).removeClass("activeBtn");
        });
        $("#" + btn).addClass("activeBtn");
    };

    Pets.getUserValidModel = function () {
        return new Backbone.Model({
            userValidate: Cookie('petsaut'),
            price: "",
            sellLoveGift: '0',
            likes: ""
        });
    }

    return Pets;
});