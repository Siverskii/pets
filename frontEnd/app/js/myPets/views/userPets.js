'use strict';
define(["app", "marionette",
        "tpl!myPets/views/tmpl/userPets.tpl"],
    function (App, Marionette, mainTpl) {
        var userPets = Marionette.View.extend({
            template: mainTpl,
            className: "col-lg-3 col-sm-4 col-xs-6 thumb2",

            events: {
                "click .petsWrapper": "openPetsDescr",
            },

            openPetsDescr: function () {
                App.trigger("userPets:openPetsDescr", this.model.get("id"));
            }
        });
        return userPets;
    });