'use strict';
define(['app', "marionette", "tpl!myPets/views/tmpl/userPetsView.tpl"], function (App, Marionette, userPetsViewTpl) {
    var userPetsView = Marionette.View.extend({
        template: userPetsViewTpl,
        regions: {
            userPetsMainRegion: "#userPetsMainRegion"
        },
        events: {
            "click  .addNewPet": "addPets_btn"
        },

        addPets_btn: function () {
            App.trigger("showPetsAddForm");
        }
    });
    return userPetsView;

});