'use strict';
define(["marionette","myPets/views/userPets",
        "myPets/views/userPetsEmpty"],
    function (Marionette,userPets, userPetsEmpty) {
        var petsCollection = Marionette.CollectionView.extend({
            className: "col-xs-10 col-xs-offset-1  userPetsCollection",
            childView: userPets,
            emptyView: userPetsEmpty
        });
        return petsCollection;
    });