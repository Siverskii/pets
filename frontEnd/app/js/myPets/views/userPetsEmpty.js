'use strict';
define(["app", "marionette",
        "tpl!myPets/views/tmpl/userPetsEmpty.tpl"],
    function (App, Marionette, emtyTpl) {
        var userPetsEmpty = Marionette.View.extend({
            className: "userPetsEmpty",
            template: emtyTpl,

            events: {
                "click  .addPets_btn": "addPets_btn"
            },

            addPets_btn: function () {
                App.trigger("showPetsAddForm");
            }
        });
        return userPetsEmpty;
    });