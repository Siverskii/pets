'use strict';
define(["app",
        "pets/header/header",
        "myPets/views/userPetsCollectionView",
        "petsForm/petsFormController",
        "myPets/views/userPetsView",
        "pets/petsData/myPetsCollection",
        "pets/petsData/petsModel"],
    function (App, Header, userPetsCollectionView, petsFormController, userPetsView, myPetsCollection, petsModel) {
        function MyPetsCotroller() {
            this.showMyPets = function () {
                if (!App.getView().getRegion("sideBarRegion").hasView()) {
                    App.getView().render();
                }
                App.trigger("startWaitingIndicator");
                var petsCollection = new myPetsCollection();
                petsCollection.fetch({
                    success: function (collection) {

                        App.getView().getRegion('headerRegion').show(new Header({
                            model: App.getUserValidModel()
                        }));
                        var userPets = new userPetsCollectionView({
                            collection: collection
                        });
                        userPets.on("dom:refresh", function () {
                            App.trigger("petsForm:alignColumn");
                            App.trigger("stopWaitingIndicator");
                        });
                        var uPetsView = new userPetsView();
                        App.getView().getRegion('mainRegion').show(uPetsView);
                        uPetsView.getRegion('userPetsMainRegion').show(userPets);
                    },
                    error: function () {
                        App.trigger("stopWaitingIndicator");
                        App.trigger("showConnectionErrorPopUp");
                    }
                });
            }
            App.on("showPetsAddForm", function () {
                petsFormController.showAddForm();
            });

            this.showPetsDescr = function (petsId) {
                if (!App.getView().getRegion("sideBarRegion").hasView()) {
                    App.getView().render();
                }
                App.trigger("startWaitingIndicator");
                new petsModel().fetch({
                    data: {
                        petsId: petsId
                    },
                    success: function (model) {
                        App.trigger("stopWaitingIndicator");
                        petsFormController.showAddForm(model);
                        App.getView().getRegion('headerRegion').show(new Header({
                            model: App.getUserValidModel()
                        }));
                    },
                    error: function () {
                        App.trigger("stopWaitingIndicator");
                        App.trigger("showConnectionErrorPopUp");
                    }
                });
            }
        }
        return new MyPetsCotroller();
    });