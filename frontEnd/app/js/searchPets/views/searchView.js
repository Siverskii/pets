'use strict';
define(['app', "marionette", "tpl!searchPets/views/tpl/searchView.tpl"], function (App, Marionette, searchForm) {
    var searchForm = Marionette.View.extend({
        template: searchForm,
        regions: {
            searchFormHeader: "#searchFormHeader",
            searchFormMain: "#searchFormMain"
        }
    });
    return searchForm;
});