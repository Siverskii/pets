'use strict';
define(["marionette",
        "searchPets/views/selectedPets"],
    function (Marionette, selectedPets) {
        var searchFormMain = Marionette.CollectionView.extend({
            className: "col-xs-10 col-xs-offset-1  userPetsCollection",
            childView: selectedPets,
            events: {

            },
        });
        return searchFormMain;
    });