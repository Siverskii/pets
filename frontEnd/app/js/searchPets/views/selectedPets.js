'use strict';
define(["app", "marionette",
        "tpl!searchPets/views/tpl/selectedPets.tpl"],
    function (App, Marionette, mainTpl) {
        var userPets = Marionette.View.extend({
            template: mainTpl,
            className: "col-lg-3 col-sm-4 col-xs-6 thumb2",

            events: {
                "click .petsWrapper": "openPetsDescr",
            },

            openPetsDescr: function () {
                App.trigger("openPetsDescription", this.model);
            }
        });
        return userPets;
    });