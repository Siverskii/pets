'use strict';
define([
        "app",
        "pets/header/header",
        "searchPets/views/searchView",
        "petsForm/petsFormController",
        "searchPets/views/searchMain"],
    function (App, Header, searchView, petsFormController, searchMain) {
        function petsSearchController() {
            var search, petsSearchController = this;
            this.showSearchForm = function () {
                if (!App.getView().getRegion("sideBarRegion").hasView()) {
                    App.getView().render();
                }
                search = new searchView();
                App.trigger("petsForm:alignColumn");
                App.getView().getRegion('mainRegion').show(search);
                App.getView().getRegion('headerRegion').show(new Header({
                    model: App.getUserValidModel()
                }));
                if (petsSearchController.collection === undefined) {
                    search.getRegion("searchFormHeader").show(petsFormController.getSerchForm());
                } else {
                    App.trigger("searchForm:gotCollection", petsSearchController.collection)
                }
            }
            App.on("searchForm:gotCollection", function (collection) {
                petsSearchController.collection = collection;
                App.trigger("stopWaitingIndicator");
                App.getView().getRegion('headerRegion').show(new Header({
                    model: App.getUserValidModel()
                }));
                var selectedPets = new searchMain({
                    collection: collection
                });
                selectedPets.on("dom:refresh", function () {
                    App.trigger("petsForm:alignColumn");
                    App.trigger("stopWaitingIndicator");
                });
                search.getRegion("searchFormMain").show(selectedPets);
            });

            App.on("sideBar:findPets_btn", function () {
                petsSearchController.collection = undefined;
            });
        }
        return new petsSearchController();
    });