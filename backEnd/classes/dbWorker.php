<?php
class dbWorker{
private $db;
private $secret = '0a9af1ce882ae15da729473dd9026123d79941e3e56408feab900422c9d7d787';

function __construct(){
    try{ 
        $dbParam = parse_ini_file('config.ini');
        $this->db = new PDO($dbParam['db.conn'],$dbParam['db.user'],$dbParam['db.pass']);
        $this->db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        $this->db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
    }catch (PDOException $e){
        return false;
    }

}

function getPets($formData=false){
    if(!$formData){
        try{ 
            $pets = $this->db -> query("SELECT pets.id, pets.catOrDog, pets.MF, pets.ownerID,  pets.breed, pets.name, pets.price,pets.sellLoveGift, pets.descr,  images.image, images.likes FROM pets,images WHERE images.pets_id = pets.id AND images.main = 1 ORDER BY RAND() LIMIT 20");//WHERE RAND() < 0.1
            
            $petsCollection = $pets->fetchAll();
            foreach($petsCollection as $key=>$pet){
                if($pet[catOrDog] == 1){
                    $petsType = "cats";
                }else{
                    $petsType = "dogs";  
                }
                $petsCollection[$key][image] = [mime_content_type("frontEnd/app/img/pets/".$petsType."/".$pet[image]),base64_encode(file_get_contents("frontEnd/app/img/pets/".$petsType."/".$pet[image]))];    
            }
           return $petsCollection;    
         }catch (PDOException $e){
            return false;
         } 
    }elseif($formData){ 
        $catDorQuery = "";
        $MFQuery = ""; 
        $breed ="";
        $city ="";
        if($formData["byPets"] == "true") $sellLoveGift[] = 1;
        if($formData["love"] == "true")  $sellLoveGift[] = 2;
        if($formData["getFree"] == "true")  $sellLoveGift[] = 3;
        count($sellLoveGift) != 0 ? $sellLoveGiftQuery = "AND pets.sellLoveGift IN(".implode(",",$sellLoveGift).")" : $sellLoveGiftQuery = "";
        
        if(!(($formData["cat"] == "true" && $formData["dog"]== "true") || ($formData["cat"] == "false" && $formData["dog"] == "false"))){
            if($formData["cat"] == "true") $catDorQuery = " AND pets.catOrDog = 1";
            if($formData["dog"] == "true") $catDorQuery = " AND pets.catOrDog = 0";     
        }
        if(!(($formData["boy"] == "true" && $formData["girl"]== "true") || ($formData["boy"] == "false" && $formData["girl"] == "false"))){
            if($formData["boy"] == "true") $MFQuery = " AND pets.MF = 0";
            if($formData["girl"] == "true") $MFQuery = " AND pets.MF = 1";  
        }
        if($formData["rate"] == "true"){
            $order = " ORDER BY images.likes DESC";  
        }else{
            $order = " ORDER BY pets.id DESC";  
        }
        
        if($formData["breed"]){
            $breed =  " AND pets.breed = :breed"; 
        }
        if($formData["city"]){
            $city = " AND pets.ownerID IN (SELECT id FROM users WHERE city = :city)";
        } 
        $query = "SELECT pets.id, pets.MF, pets.catOrDog,  pets.breed, pets.name, pets.price, pets.sellLoveGift,pets.ownerID,  images.image, images.likes FROM pets,images WHERE images.pets_id = pets.id AND images.main = 1 ".$sellLoveGiftQuery.$catDorQuery.$MFQuery.$breed.$city.$order." LIMIT 100";
    
        try{
            $stmt = $this-> db -> prepare($query);
            if($formData["breed"]){
                $stmt -> bindParam(':breed',$formData["breed"]);
            }
            if($formData["city"]){
                $stmt -> bindParam(':city',$formData["city"]);
            }
            $stmt -> execute();  
            
            $petsCollection = $stmt -> fetchAll();
            foreach($petsCollection as $key=>$pet){
                if($pet[catOrDog] == 1){
                    $petsType = "cats";
                }else{
                    $petsType = "dogs";  
                }
                $petsCollection[$key][image] = [mime_content_type("frontEnd/app/img/pets/".$petsType."/".$pet[image]),base64_encode(file_get_contents("frontEnd/app/img/pets/".$petsType."/".$pet[image]))];      
            }
            return   $petsCollection;    
         }catch (PDOException $e){
             return "dbError";
        }        
    }
} 

function getMyPets($uID){
    try{
        $pets = $this->db -> query("SELECT pets.id, pets.catOrDog, pets.name, pets.price, pets.sellLoveGift, images.image, images.likes FROM pets,images WHERE images.pets_id = pets.id AND images.main = 1 AND ownerID =".$uID);
        $petsCollection = $pets->fetchAll();
        foreach($petsCollection as $key=>$pet){
                if($pet[catOrDog] == 1){
                    $petsType = "cats";
                }else{
                    $petsType = "dogs";  
                }
                $petsCollection[$key][image] = [mime_content_type("frontEnd/app/img/pets/".$petsType."/".$pet[image]),base64_encode(file_get_contents("frontEnd/app/img/pets/".$petsType."/".$pet[image]))];      
            }
         return $petsCollection;
     }catch (PDOException $e){
         return false;
    }
}
    
function addLikeToImg($petsId){
    $query = "UPDATE images SET likes = likes + 1 WHERE pets_id = :petsId AND main = 1";
    try{
        $stmt = $this->db->prepare($query);
        $stmt->bindParam(':petsId',$petsId);
        return $stmt->execute();
    }catch (PDOException $e){
        return false;
    }   
}
    
function isUserExists($userEmail){
    $query = "SELECT id FROM users WHERE  users.email = :userEmail";
    try{
        $stmt = $this-> db -> prepare($query);
        $stmt -> bindParam(':userEmail',$userEmail);
        $stmt -> execute();  
        return  $stmt -> fetch();   
     }catch (PDOException $e){
         return "dbError";
    }   
}
    
function addNewUser($email,$password){
    $shaPassword = hash_hmac('sha256',$password,$this->secret);
    $addNewUser = $this->db->prepare("INSERT INTO users (email, password) 
                                                VALUES (:email,:password)");
    $addNewUser->bindParam(':email',$email);
    $addNewUser->bindParam(':password',$shaPassword);
    if ($addNewUser->execute()){
      return $this->db->lastInsertId();  
    }else {
        return false;
    }    
}
    
function userConfirm($email, $password){
    $shaPassword = hash_hmac('sha256',$password,$this->secret);
    $query = "SELECT id FROM users WHERE email=:userEmail AND password=:userPassword";
    try{
        $stmt = $this->db->prepare($query);
        $stmt -> bindParam(':userEmail',$email);
        $stmt -> bindParam(':userPassword', $shaPassword);
        $stmt -> execute();
        $row = $stmt->fetch();  
        return $row['id'];
    }catch (PDOException $e){
        return "dbError";
    }     	 
}

function getPetDescription($petsId){
    $query = "SELECT id, catOrDog, MF, breed, name, sellLoveGift, descr, price, ownerID FROM pets WHERE id =:petsId";       
    $query2 = "SELECT image, likes, main FROM images WHERE images.pets_id = :petsId";
    try{
        $stmt = $this->db -> prepare($query);
        $stmt2 = $this->db -> prepare($query2);
        $stmt->bindParam(':petsId',$petsId);
        $stmt2->bindParam(':petsId',$petsId);
        $stmt->execute();
        $stmt2->execute();
        $model = $stmt->fetch();
        $images = $stmt2->fetchAll();
        $model["images"] = $images;
        return $model;
    }catch (PDOException $e){
        return false;
   }  
}
    
function savePet($content,$savedImage,$uID){
    $breed = filter_var($content['breed'], FILTER_SANITIZE_STRING);
    $imageArray = $content['img'];
    $catOrDog = (filter_var($content['catsOrDogs'], FILTER_SANITIZE_STRING));
    $MF = filter_var($content['MF'], FILTER_SANITIZE_STRING);
    $name = filter_var($content['name'], FILTER_SANITIZE_STRING);
    $sellLoveGift = filter_var($content['sellLoveGift'], FILTER_SANITIZE_STRING);
    $price = filter_var($content['price'], FILTER_SANITIZE_STRING);
    $lovePrice = filter_var($content['lovePrice'], FILTER_SANITIZE_STRING);
    $descr = filter_var($content['descr'], FILTER_SANITIZE_STRING);
    
    $query = "INSERT INTO pets (catOrDog, MF,breed,name,price, sellLoveGift,descr,ownerID) 
                                        VALUES (:catOrDog, :MF,:breed,:name,:price,:sellLoveGift,:descr,:ownerID)";
    $query2 = "INSERT INTO images (image,main,pets_id) VALUES (:image,:main,:petsId)";
    
    try{
        $stmt = $this->db -> prepare($query);
        $stmt2 = $this->db -> prepare($query2);
        $stmt->bindParam(':catOrDog',$catOrDog);
        $stmt->bindParam(':MF',$MF);
        $stmt->bindParam(':breed',$breed);
        $stmt->bindParam(':name',$name);
        $stmt->bindParam(':sellLoveGift',$sellLoveGift);
        $stmt->bindParam(':price',$price);
        $stmt->bindParam(':descr',$descr);
        $stmt->bindParam(':ownerID',$uID);
        $stmt -> execute();
        $petsId = $this->db ->lastInsertId();

         foreach($savedImage as $im){
             $imageDetail = explode("/",$im);
             $stmt2->bindParam(':image',$imageDetail[0]); 
             $stmt2->bindParam(':main',$imageDetail[1]);
             $stmt2->bindParam(':petsId', $petsId);
             $stmt2 -> execute();   
         }
        return true;
    }catch (PDOException $e){
        return false;
   }                          
}
    
function updatePet($content,$uID,$savedImage){
    $breed = filter_var($content['breed'], FILTER_SANITIZE_STRING);
    $catOrDog = (filter_var($content['catsOrDogs'], FILTER_SANITIZE_STRING));
    $MF = filter_var($content['MF'], FILTER_SANITIZE_STRING);
    $name = filter_var($content['name'], FILTER_SANITIZE_STRING);
    $sellLoveGift = filter_var($content['sellLoveGift'], FILTER_SANITIZE_STRING);
    $price = filter_var($content['price'], FILTER_SANITIZE_STRING);
    $descr = filter_var($content['descr'], FILTER_SANITIZE_STRING);
    $petsId = filter_var($content['id'], FILTER_SANITIZE_STRING);
    $mainImg = filter_var($content['mainImg'], FILTER_SANITIZE_STRING);
    
    $queryUpdateInfo = "UPDATE pets SET catOrDog = :catOrDog, MF = :MF, breed = :breed, name = :name, sellLoveGift = :sellLoveGift, price = :price, descr = :descr,  ownerID = :ownerID WHERE id = :petsId"; 
    $queryInsetImg = "INSERT INTO images (image,main,pets_id) VALUES (:image,:main,:petsId)";
    $queryDeleteImg = "DELETE FROM images WHERE image =:image";
    $queryUnsetMainImg = "UPDATE images SET main = 0 WHERE pets_id = :petsId";
    $querySetMainImg = "UPDATE images SET main = 1 WHERE image = :mainImg";
    
    try{
        $stmtInfo = $this->db -> prepare($queryUpdateInfo);
        $stmtUnsetMainImg = $this->db -> prepare($queryUnsetMainImg);
        
        $stmtInfo->bindParam(':catOrDog',$catOrDog);
        $stmtInfo->bindParam(':MF',$MF);
        $stmtInfo->bindParam(':breed',$breed);
        $stmtInfo->bindParam(':name',$name);
        $stmtInfo->bindParam(':sellLoveGift',$sellLoveGift);
        $stmtInfo->bindParam(':price',$price);
        $stmtInfo->bindParam(':descr',$descr);
        $stmtInfo->bindParam(':ownerID',$uID);
        $stmtInfo->bindParam(':petsId',$petsId);
        $stmtInfo->execute();
        
        $stmtUnsetMainImg->bindParam(':petsId',$petsId);
        $stmtUnsetMainImg->execute();
    
        if($mainImg){
            $stmtSetMainImg = $this->db -> prepare($querySetMainImg);
            $stmtSetMainImg->bindParam(':mainImg',$mainImg);
            $stmtSetMainImg->execute();    
        }

        if($savedImage){
            $stmtInsetImg = $this->db -> prepare($queryInsetImg);
            foreach($savedImage as $img){
                $imageDetail = explode("/",$img);
                $stmtInsetImg->bindParam(':image',$imageDetail[0]); 
                $stmtInsetImg->bindParam(':main',$imageDetail[1]);
                $stmtInsetImg->bindParam(':petsId', $petsId);
                $stmtInsetImg->execute();   
             }  
        }
        return true;
    }catch (PDOException $e){
        return false;
   }
}
    
function deletePet($petsId){
    $query = "SELECT catOrDog FROM pets WHERE id =:petsId";
    $query2 = "SELECT image FROM images WHERE pets_id =:petsId";
    $deleteQuery = "DELETE FROM pets WHERE id =:petsId";
    $deleteQuery2 = "DELETE FROM images WHERE pets_id =:petsId";
    
    try{
        $stmt = $this->db -> prepare($query);
        $stmt2 = $this->db -> prepare($query2); 
        $deleteStmt = $this->db -> prepare($deleteQuery);
        $deleteStmt2 = $this->db -> prepare($deleteQuery2);
        $stmt->bindParam(':petsId',$petsId);
        $stmt2->bindParam(':petsId',$petsId);
        $deleteStmt->bindParam(':petsId',$petsId);
        $deleteStmt2->bindParam(':petsId',$petsId);
        $stmt->execute();
        $stmt2->execute();
        $deleteStmt->execute();
        $deleteStmt2->execute();
        $catOrDog = $stmt->fetch();
        $images = $stmt2->fetchAll();
     }catch (PDOException $e){
        return false; 
    } 
    if($catOrDog[catOrDog] == 1){
        $petsType = "cats";
    }else{
        $petsType = "dogs";  
    }
    
     foreach($images as $imgName){
          unlink("frontEnd/app/img/pets/".$petsType."/".$imgName[image]);   
          unlink("frontEnd/app/img/pets/".$petsType."_thumb/".$imgName[image]);   
     }
    return true;
}

function deletePetsImage($imageArray, $catOrDog){
    if($catOrDog == 1){
        $petsType = "cats";
    }else{
        $petsType = "dogs";  
    }
    $delete = "DELETE FROM images WHERE image =:image";
    try{
        $stmt = $this->db -> prepare($delete);
        foreach($imageArray as $imgName){
            $stmt->bindParam(':image',$imgName);
            $stmt->execute();
            unlink("frontEnd/app/img/pets/".$petsType."/".$imgName);   
            unlink("frontEnd/app/img/pets/".$petsType."_thumb/".$imgName);
        }
        return true;
     }catch (PDOException $e){
        return false; 
    }       
}
    
function getUserInfo($uID){
    try{
        $data = $this->db -> query("SELECT id, email, phone, emailEn, phoneEn, name, city FROM users WHERE id =".$uID);
        $pets = $this->db -> query("SELECT id FROM pets WHERE ownerID =".$uID);
        $userInfo = $data->fetch();
        $userPets = $pets->fetchAll();
        $userInfo["pets"] = count($userPets);
        return $userInfo;   
     }catch (PDOException $e){
         return false;
    }  
}
       
function updateUserInfo($uID,$content){
    $email = filter_var($content['email'], FILTER_SANITIZE_STRING);
    $name = filter_var($content['name'], FILTER_SANITIZE_STRING);
    $city = filter_var($content['city'], FILTER_SANITIZE_STRING);
    $phone = filter_var($content['phone'], FILTER_SANITIZE_STRING);
    $phoneCon = filter_var($content['phoneCon'], FILTER_SANITIZE_STRING);
    $emailCon = filter_var($content['emailCon'], FILTER_SANITIZE_STRING);
    $password = filter_var($content['password'], FILTER_SANITIZE_STRING);
    
    $queryUpdateInfo = "UPDATE users SET email =:email, name =:name, city=:city, phone=:phone, phoneEn=:phoneCon,emailEn=:emailCon,password=:password  WHERE id = :userId"; 
    $queryUpdateInfoWithoutPassword = "UPDATE users SET email =:email, name =:name, city=:city, phone=:phone, phoneEn=:phoneCon,emailEn=:emailCon WHERE id = :userId"; 
    
    try{
        if($password){
            $stmt = $this->db -> prepare($queryUpdateInfo); 
            $stmt->bindParam(':password',hash_hmac('sha256',$password,$this->secret));
        }else{
            $stmt = $this->db -> prepare($queryUpdateInfoWithoutPassword);    
        }
        $stmt->bindParam(':email',$email);
        $stmt->bindParam(':name',$name);
        $stmt->bindParam(':city',$city);
        $stmt->bindParam(':phone',$phone);
        $stmt->bindParam(':phoneCon',$phoneCon);
        $stmt->bindParam(':emailCon',$emailCon);
        $stmt->bindParam(':userId',$uID);
        return $stmt->execute();  
    }catch (PDOException $e){
        return false;
   } 
}
    
function getAllPetImage($petsId){   
    $query = "SELECT image, likes, main FROM images WHERE images.pets_id = :petsId";
    try{
        $stmt = $this->db -> prepare($query);
        $stmt->bindParam(':petsId',$petsId);
        $stmt->execute();
        return $images = $stmt->fetchAll();
    }catch (PDOException $e){
        return false;
   }    
}
function getUserContact($userId){   
    $query = "SELECT email, phone, emailEn, phoneEn, city FROM users WHERE id = :userId";
    try{
        $stmt = $this->db -> prepare($query);
        $stmt->bindParam(':userId',$userId);
        $stmt->execute();
        return $contacts = $stmt->fetch();
    }catch (PDOException $e){
        return false;
   }    
}
                
}