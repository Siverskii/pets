<?php
class petsFunc{

function tokenConfirm($token){
    $JWT = explode(".",$token);
    $secret = 'da3046119b2d23b71bac77c2f742d0bf2e898432bd3f0fa85001e12c1ec5a6f1';
    if ($JWT[2] == (hash_hmac('sha256',$JWT[0].'.'.$JWT[1],$secret))){
        $payload = json_decode(base64_decode($JWT[1]));
        /*$userData[]=$payload -> uID;
        $userData[]=$payload -> email;*/
        return  $payload;
    }else return false;
}
    
function generateJSWT($email, $userID){ 
    $header = base64_encode(json_encode(array(
        'alg'=> 'HS256',
        'typ' => 'JWT'
    )));
    $payload = base64_encode(json_encode(array(
        'uID' => $userID,
        'email' => $email
    )));
    $secret = 'da3046119b2d23b71bac77c2f742d0bf2e898432bd3f0fa85001e12c1ec5a6f1';
    $signature = hash_hmac('sha256',$header.'.'.$payload,$secret);
    return $header.'.'.$payload.'.'.$signature;      
}
    
function getDataFromFile($file){
    $data = file("backEnd/classes/".$file);
    if ($data){
        $dataArray = array();
        foreach($data as $key => $value){
            $dataArray[$key]["data"] = $value;    
        } 
        return  $dataArray;    
    }else{
       return  false;     
    }
}
    
function savePetsImage($imageArray, $uID, $catOrDog){
    $savedImage =[];
    foreach($imageArray as $num => $im){
        $rawImge = explode(";",$im);
        $rawtype = explode("/",$rawImge[0]);
        $type = $rawtype[1];
        $rawimg = explode(",",$rawImge[1]);
        $img = base64_decode($rawimg[1]);
        $saveImage = 'image'.$type;
        if($catOrDog){
            $petsType = "cats";
        }else{
            $petsType = "dogs";  
        }
        $imgName = time().$uID.$num;
        if($image = imagecreatefromstring($img)){
            $width = ImageSX($image);
            $heigth = ImageSY($image);
            $ratio = $width/$heigth;
            if ($width > 800){
                $width = 800;
                $heigth = round(800/$ratio);
            }
            $thumbWidth = 192;
            $thumbHeight = 192/$ratio;
          
            $newResolutionImg=imagecreatetruecolor($width,$heigth);
            imagecopyresampled($newResolutionImg,$image,0,0,0,0,$width,$heigth,ImageSX($image),ImageSY($image));
            
            $newResolutionThumbImg=imagecreatetruecolor($thumbWidth,$thumbHeight);
            imagecopyresampled($newResolutionThumbImg,$image,0,0,0,0,$thumbWidth,$thumbHeight,ImageSX($image),ImageSY($image));
           if($saveImage($newResolutionImg,"frontEnd/app/img/pets/".$petsType."/".$imgName.".".$type) && $saveImage($newResolutionThumbImg,"frontEnd/app/img/pets/".$petsType."_thumb/".$imgName.".".$type)){
              if($rawimg[2] == "true"){
                  $main = 1;
              }else $main = 0; 
             
                $savedImage[] = $imgName.".".$type.'/'.$main;
                imagedestroy($image);
                imagedestroy($newResolutionImg);  
           }   
        }
    }
    return $savedImage;
}
     
}